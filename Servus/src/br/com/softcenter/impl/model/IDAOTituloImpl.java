package br.com.softcenter.impl.model;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softcenter.config.connection.ConnectionConfig;
import br.com.softcenter.impl.model.bean.Cliente;
import br.com.softcenter.impl.model.bean.Origem;
import br.com.softcenter.impl.model.bean.SegmentoT;
import br.com.softcenter.impl.model.bean.Titulo;
import br.com.softcenter.utils.database.DatabaseUtils;
import br.com.softcenter.utils.date.DateUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since  10-09-2015
 */
public class IDAOTituloImpl implements IDAOModel<Titulo, Long> {

	private Connection conn;
	private PreparedStatement pstmt;
	
	public IDAOTituloImpl() {
		conn = ConnectionConfig.getConn();
	}
	
	@Override
	public List<Titulo> select() {
		String sql = "SELECT * FROM titulo";
		try{
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			List<Titulo> titulos = new ArrayList<Titulo>();
			
			while(rs.next()){
				titulos.add(select(rs.getLong("TIT_ID")));
			}
			return titulos; 
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}	
		return null;
	}

	@Override
	public Titulo select(Long key) {
		String sql = "SELECT titulo.*, origem.org_ID, origem.ORG_DESCRICAO, cliente.CLN_ID, " 		+ 
					 "		 cliente.CLN_RAZAOSOCIAL FROM apr.titulo " 						  		+		
					 "LEFT JOIN apr.origem  ON (origem.ORG_ID = titulo.ORG_ID) " 			  		+
					 "LEFT JOIN apr.cliente ON (cliente.CLN_ID = titulo.CLN_ID)  WHERE TIT_ID = ? ";
		ResultSet rs = null;
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, key);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				Cliente cliente = new Cliente(rs.getLong("CLN_ID"), rs.getString("CLN_RAZAOSOCIAL"));
				Origem origem  	= new Origem(rs.getString("ORG_DESCRICAO"), rs.getLong("org_ID"));
				
				Titulo titulo = new Titulo(rs.getLong("TIT_ID"), 
						rs.getLong("TIT_PARCELA"), 
						rs.getString("TIT_DESCRICAO"), 
						DateUtils.ConvertStringToUtilDate(rs.getString("TIT_DATALANCAMENTO")), 
						DateUtils.ConvertStringToUtilDate(rs.getString("TIT_DATAEMISSAO")), 
						DateUtils.ConvertStringToUtilDate(rs.getString("TIT_DATAVENCIMENTO")), 
						new BigDecimal(rs.getString("TIT_VALOR")), 
						new BigDecimal(rs.getString("TIT_SALDO")), 
						rs.getString("TIT_SITUACAO").toCharArray()[0], 
						new BigDecimal(rs.getString("TIT_JURO")), 
						new BigDecimal(rs.getString("TIT_DESCONTO")),
						rs.getLong("ORG_ID"),
						rs.getLong("TIT_NUMERO"),
						origem, cliente);
				
				rs.close();
				pstmt.close();
				
				return titulo;
			}
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}catch(ParseException parseex){
			parseex.printStackTrace();
		}
		
		return null;
	}

	@Override
	public Titulo select(Long key, String where) {
		return null;
	}
	
	@Override
	public void insert(SegmentoT segmento){	}

	@Override
	public void update(SegmentoT segmento) {
		String nossoNumero = segmento.getIdentificacaoNossoNumero();
		
		if(nossoNumero != null){
//			Long titulo = DatabaseUtils.RetornarIdTituloPorNossoNumero(new Long(nossoNumero));
			Long ultimoRegistro = DatabaseUtils.RetornarUltimoValorDaTabela("titulobaixa", "TIB_ID");
			BigDecimal saldo = DatabaseUtils.RetornarCalculoSaldoTitulo(segmento.getTitID(), ultimoRegistro);
			Character situacao = ' ';
			segmento.setSaldo(saldo.abs());
			
			if(saldo.longValue() <= 0)
				situacao = 'Q';
			else
				situacao = 'F';
			
			segmento.setTipo(situacao.toString());
			
			String sql = "UPDATE apr.titulo SET apr.titulo.TIT_SALDO = ?, apr.titulo.TIT_SITUACAO = ? WHERE apr.titulo.TIT_ID = ?";
			try{
				PreparedStatement pstmt = ConnectionConfig.getConn().prepareStatement(sql);
				pstmt.setBigDecimal(1, saldo);
				pstmt.setString(2, situacao.toString());
				pstmt.setLong(3, segmento.getTitID());
				pstmt.executeUpdate();
				
			}catch(SQLException sqlex){
				sqlex.printStackTrace();
			}
		}
	}
}
