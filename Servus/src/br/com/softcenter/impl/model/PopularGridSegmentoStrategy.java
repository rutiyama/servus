package br.com.softcenter.impl.model;

import java.util.List;

import br.com.softcenter.impl.model.bean.GridFaturasJavaFXBean;


/**
 * @author Rodrigo Junior Utiyama
 * @since  09-09-2015
 */

public interface PopularGridSegmentoStrategy {
	public void popularGridRetornoJavaFx(GridFaturasJavaFXBean gridFaturasBean, List<String>  duplicados);
}
