package br.com.softcenter.impl.model.bean;

import java.math.BigDecimal;
import java.util.Date;

import javafx.beans.property.SimpleObjectProperty;
import br.com.softcenter.utils.date.DateUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since  08-09-2015
 */

public class SegmentoU extends Segmento{
	private SimpleObjectProperty<BigDecimal> jurosMultasEncargos;
	private SimpleObjectProperty<BigDecimal> valorPagoSacado;
	private SimpleObjectProperty<Date> dataEfetivacaoCredito; 
	private SimpleObjectProperty<BigDecimal> valorDesconto;
	
	private SimpleObjectProperty<Date> dataLiquidacao;
	
	private SegmentoU(){
	}
	
	public SegmentoU(BigDecimal jurosMultasEncargos,
			BigDecimal valorPagoSacado, BigDecimal valorDesconto, Date dataEfetivacaoCredito, Date dataLiquidacao) {
		this();
		
		this.jurosMultasEncargos = new SimpleObjectProperty<BigDecimal>();
		this.jurosMultasEncargos.set(jurosMultasEncargos);
		
		this.valorPagoSacado = new SimpleObjectProperty<BigDecimal>();
		this.valorPagoSacado.set(valorPagoSacado);
		
		this.valorDesconto = new SimpleObjectProperty<BigDecimal>();
		this.valorDesconto.set(valorDesconto);
		
		this.dataEfetivacaoCredito = new SimpleObjectProperty<Date>();
		this.dataEfetivacaoCredito.set(dataEfetivacaoCredito);
		
		this.dataLiquidacao = new SimpleObjectProperty<Date>();
		this.dataLiquidacao.set(dataLiquidacao);
	}

	//Getter-Setter Property
	public SimpleObjectProperty<BigDecimal> getValorDescontoProperty() {
		return valorDesconto;
	}
	public void setValorDescontoProperty(SimpleObjectProperty<BigDecimal> valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public SimpleObjectProperty<BigDecimal> getJurosMultasEncargosProperty() {
		return jurosMultasEncargos;
	}
	public SimpleObjectProperty<BigDecimal> getValorPagoSacadoProperty() {
		return valorPagoSacado;
	}
	public SimpleObjectProperty<Date> getDataEfetivacaoCreditoProperty() {
		return dataEfetivacaoCredito;
	}
	public void setJurosMultasEncargosProperty(SimpleObjectProperty<BigDecimal>  jurosMultasEncargos) {
		this.jurosMultasEncargos = jurosMultasEncargos;
	}
	public void setValorPagoSacadoProperty(SimpleObjectProperty<BigDecimal> valorPagoSacado) {
		this.valorPagoSacado = (valorPagoSacado);
	}
	public void setDataEfetivacaoCreditoProperty(
			SimpleObjectProperty<Date> dataEfetivacaoCredito) {
		this.dataEfetivacaoCredito = dataEfetivacaoCredito;
	}
	
	//Getter-Setter padrão:
	public BigDecimal getJurosMultasEncargos() {
		return jurosMultasEncargos.get();
	}
	public BigDecimal getValorPagoSacado() {
		return valorPagoSacado.get();
	}
	public Date getDataEfetivacaoCredito() {
		return dataEfetivacaoCredito.get();
	}
	public void setJurosMultasEncargos(BigDecimal jurosMultasEncargos) {
		this.jurosMultasEncargos.set(jurosMultasEncargos);
	}
	public void setValorPagoSacado(BigDecimal valorPagoSacado) {
		this.valorPagoSacado.set(valorPagoSacado);
	}
	public void setDataEfetivacaoCredito(Date dataEfetivacaoCredito) {
		this.dataEfetivacaoCredito.set(dataEfetivacaoCredito);
	}
	public BigDecimal getValorDesconto() {
		return valorDesconto.get();
	}
	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = new SimpleObjectProperty<BigDecimal>(valorDesconto);
	}

	public SimpleObjectProperty<Date> getDataLiquidacao() {
		return dataLiquidacao;
	}

	public void setDataLiquidacao(Date dataLiquidacao) {
		this.dataLiquidacao = new SimpleObjectProperty<Date>(dataLiquidacao);
	}
	
	@Override
	public String toString(){
		return "018...032 [Juros/Multa/Encargos] " + this.jurosMultasEncargos + "\n" +
			   "078...092 [Valor Pago Sacado]    " + this.valorPagoSacado	  + "\n" +	
			   "146...153 [Data Efetiv. Crédito] " + DateUtils.ConvertDateUtilToString(getDataEfetivacaoCredito()) + "\n\n"; 
	}

	public void setDataLiquidacao(SimpleObjectProperty<Date> dataLiquidacao) {
		this.dataLiquidacao = dataLiquidacao;
	}

}
