package br.com.softcenter.impl.model.bean;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Rodrigo Junior Utiyama
 * @since  02-09-2015
 */


public class Conta { 
	private SimpleLongProperty id;
	private SimpleStringProperty descricao;
	private SimpleStringProperty tipo;
	private SimpleLongProperty codigoContabil;
	private SimpleStringProperty numeroBanco;
	
	public SimpleStringProperty numeroBanco(){
		return this.numeroBanco;
	}
	
	public SimpleLongProperty idProperty(){
		return id;
	}
	public SimpleStringProperty descricaoProperty(){
		return descricao;
	}
	public SimpleStringProperty tipoProperty(){
		return tipo;
	}
	public SimpleLongProperty codigoContabil(){
		return codigoContabil;
	}
	
	public void setNumeroBancoProperty(SimpleStringProperty numeroBanco){
		this.numeroBanco = numeroBanco;
	}
	
	/*
	 * 
	public void setIdProperty(LongProperty id) {
		this.id = id;
	}
	public void setDescricaoProperty(StringProperty descricao) {
		this.descricao = descricao;
	}
	public void setTipoProperty(StringProperty tipo) {
		this.tipo = tipo;
	}
	public void setCodigoContabilProperty(LongProperty codigoContabil) {
		this.codigoContabil = codigoContabil;
	}
	public LongProperty getIdProperty() {
		return id;
	}
	public StringProperty getDescricaoProperty() {
		return descricao;
	}
	public StringProperty getTipoProperty() {
		return tipo;
	}
	public LongProperty getCodigoContabilProperty() {
		return codigoContabil;
	}
	 */
	
	
	public void setNumeroBanco(String numeroBanco){
		this.numeroBanco = new SimpleStringProperty(numeroBanco);
	}
	
	public String getNumeroBanco(){
		return this.numeroBanco.get();
	}
	
	public SimpleStringProperty getNumeroBancoProperty() {
		return numeroBanco;
	}

	public long getId() {
		return id.get();
	}
	public String getDescricao() {
		return descricao.get();
	}
	public String getTipo() {
		return tipo.get();
	}

	public long getCodigoContabil() {
		return codigoContabil.get();
	}

	
	public void setId(long id) {
		this.id = new SimpleLongProperty(id);
	}
	public void setTipo(String tipo) {
		this.tipo = new SimpleStringProperty(tipo);
	}
	public void setCodigoContabil(long codigoContabil) {
		this.codigoContabil = new SimpleLongProperty(codigoContabil);
	}
	public void setDescricao(String descricao) {
		this.descricao = new SimpleStringProperty(descricao);
	}
	@Override
	public String toString() {
		return "Conta [id=" + id + ", descricao=" + descricao + ", tipo="
				+ tipo + ", codigoContabil=" + codigoContabil + "]";
	}
}
