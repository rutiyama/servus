package br.com.softcenter.impl.model.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */

public class ContaMovimento {
	private Long orgID;
	private Long documento;
	private String tipoMovimento;
	private String tipo;
	private String historico;
	private BigDecimal saldoDia;
	private BigDecimal valorSacado;
	private Date data;
	
	public ContaMovimento(){}
	
	public ContaMovimento(Long orgID, Long documento, String tipoMovimento,
			String tipo, String historico, BigDecimal saldoDia, Date data) {
		this.orgID = orgID;
		this.documento = documento;
		this.tipoMovimento = tipoMovimento;
		this.tipo = tipo;
		this.historico = historico;
		this.saldoDia = saldoDia;
		this.data = data;
	}

	public BigDecimal getValorSacado() {
		return valorSacado;
	}
	public void setValorSacado(BigDecimal valorSacado) {
		this.valorSacado = valorSacado;
	}
	public BigDecimal getSaldoDia() {
		return saldoDia;
	}
	public void setSaldoDia(BigDecimal saldoDia) {
		this.saldoDia = saldoDia;
	}
	public Long getOrgID() {
		return orgID;
	}
	public Long getDocumento() {
		return documento;
	}
	public String getTipoMovimento() {
		return tipoMovimento;
	}
	public String getTipo() {
		return tipo;
	}
	public String getHistorico() {
		return historico;
	}
	public void setOrgID(Long orgID) {
		this.orgID = orgID;
	}
	public void setDocumento(Long documento) {
		this.documento = documento;
	}
	public void setTipoMovimento(String tipoMovimento) {
		this.tipoMovimento = tipoMovimento;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
}
