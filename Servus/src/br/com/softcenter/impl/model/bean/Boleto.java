package br.com.softcenter.impl.model.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since  10-09-2015
 */
public class Boleto extends BeanRetorno{
	private long id;
	private Titulo titulo;
	private Date dataEmissao;
	private BigDecimal valorTitulo;
	private BigDecimal valorTaxa;
	private BigDecimal nossoNumero;
	
	public Boleto(long id, Date dataEmissao,
			BigDecimal valorTitulo, BigDecimal valorTaxa, BigDecimal nossoNumero) {
		super();
		this.id = id;
		this.dataEmissao = dataEmissao;
		this.valorTitulo = valorTitulo;
		this.valorTaxa = valorTaxa;
		this.nossoNumero = nossoNumero;
	}
	
	public Boleto(){}

	public long getId() {
		return id;
	}

	public Titulo getTitulo() {
		return titulo;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public BigDecimal getValorTitulo() {
		return valorTitulo;
	}

	public BigDecimal getValorTaxa() {
		return valorTaxa;
	}

	public BigDecimal getNossoNumero() {
		return nossoNumero;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setTitulo(Titulo titulo) {
		this.titulo = titulo;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public void setValorTitulo(BigDecimal valorTitulo) {
		this.valorTitulo = valorTitulo;
	}

	public void setValorTaxa(BigDecimal valorTaxa) {
		this.valorTaxa = valorTaxa;
	}

	public void setNossoNumero(BigDecimal nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	@Override
	public String toString() {
		return "Boleto [id=" + id + ", titulo=" + titulo + ", dataEmissao="
				+ dataEmissao + ", valorTitulo=" + valorTitulo + ", valorTaxa="
				+ valorTaxa + ", nossoNumero=" + nossoNumero + "]";
	}
}
