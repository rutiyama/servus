package br.com.softcenter.impl.model.bean;

import java.io.Serializable;

/**
 * @author Rodrigo Junior Utiyama
 * @since  03-09-2015
 */

public class BeanIniUtils implements Serializable{
	private String database_url;
	private String database_user;
	private String database_pass;
	private String database_encoding;
	
	public BeanIniUtils(){}
	
	
	public BeanIniUtils(String database_url, String database_user,
			String database_pass, String database_encoding) {
		super();
		this.database_url = database_url;
		this.database_user = database_user;
		this.database_pass = database_pass;
		this.database_encoding = database_encoding;
	}
 
	public String getDatabase_url() {
		return database_url;
	}
	public void setDatabase_url(String database_url) {
		this.database_url = database_url;
	}
	public String getDatabase_user() {
		return database_user;
	}
	public void setDatabase_user(String database_user) {
		this.database_user = database_user;
	}
	public String getDatabase_pass() {
		return database_pass;
	}
	public void setDatabase_pass(String database_pass) {
		this.database_pass = database_pass;
	}
	public String getDatabase_encoding() {
		return database_encoding;
	}
	public void setDatabase_encoding(String database_encoding) {
		this.database_encoding = database_encoding;
	}

	@Override
	public String toString() {
		return "BeanIniUtils [database_url=" + database_url
				+ ", database_user=" + database_user + ", database_pass="
				+ database_pass + ", database_encoding=" + database_encoding
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((database_encoding == null) ? 0 : database_encoding
						.hashCode());
		result = prime * result
				+ ((database_pass == null) ? 0 : database_pass.hashCode());
		result = prime * result
				+ ((database_url == null) ? 0 : database_url.hashCode());
		result = prime * result
				+ ((database_user == null) ? 0 : database_user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BeanIniUtils other = (BeanIniUtils) obj;
		if (database_encoding == null) {
			if (other.database_encoding != null)
				return false;
		} else if (!database_encoding.equals(other.database_encoding))
			return false;
		if (database_pass == null) {
			if (other.database_pass != null)
				return false;
		} else if (!database_pass.equals(other.database_pass))
			return false;
		if (database_url == null) {
			if (other.database_url != null)
				return false;
		} else if (!database_url.equals(other.database_url))
			return false;
		if (database_user == null) {
			if (other.database_user != null)
				return false;
		} else if (!database_user.equals(other.database_user))
			return false;
		return true;
	}
	
	 
	
}
