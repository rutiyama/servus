package br.com.softcenter.impl.model.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since  10-09-2015
 */

public class Titulo {
	private long id;
	private long parcela;
	private String descricao;
	private Date dataLancamento;
	private Date dataEmissao;
	private Date dataVencimento;
	private BigDecimal valor;
	private BigDecimal saldo;
	private Character situacao;
	private BigDecimal juro;
	private BigDecimal desconto;
	private Long orgID;
	private Long numeroTitulo;
	private Origem origem;
	private Cliente cliente;
	private ContaMovimento contaMovimento;
	
	public Titulo(){
	}
	
	public Titulo(long id, long parcela, String descricao, Date dataLancamento,
			Date dataEmissao, Date dataVencimento, BigDecimal valor,
			BigDecimal saldo, Character situacao, BigDecimal juro,
			BigDecimal desconto, Long orgID, Long numeroTitulo) {
		super();
		this.id = id;
		this.parcela = parcela;
		this.descricao = descricao;
		this.dataLancamento = dataLancamento;
		this.dataEmissao = dataEmissao;
		this.dataVencimento = dataVencimento;
		this.valor = valor;
		this.saldo = saldo;
		this.situacao = situacao;
		this.juro = juro;
		this.desconto = desconto;
		this.orgID = orgID;
		this.numeroTitulo = numeroTitulo;
	}
	
	public Titulo(long id, long parcela, String descricao, Date dataLancamento,
			Date dataEmissao, Date dataVencimento, BigDecimal valor,
			BigDecimal saldo, Character situacao, BigDecimal juro,
			BigDecimal desconto, Long orgID, Long numeroTitulo, Origem origem,
			Cliente cliente) {
		this.id = id;
		this.parcela = parcela;
		this.descricao = descricao;
		this.dataLancamento = dataLancamento;
		this.dataEmissao = dataEmissao;
		this.dataVencimento = dataVencimento;
		this.valor = valor;
		this.saldo = saldo;
		this.situacao = situacao;
		this.juro = juro;
		this.desconto = desconto;
		this.orgID = orgID;
		this.numeroTitulo = numeroTitulo;
		this.origem = origem;
		this.cliente = cliente;
	}
	
	public ContaMovimento getContaMovimento() {
		return contaMovimento;
	}

	public void setContaMovimento(ContaMovimento contaMovimento) {
		this.contaMovimento = contaMovimento;
	}

	public Origem getOrigem() {
		return origem;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setOrigem(Origem origem) {
		this.origem = origem;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Long getOrgID() {
		return orgID;
	}

	public Long getNumeroTitulo() {
		return numeroTitulo;
	}

	public void setOrgID(Long orgID) {
		this.orgID = orgID;
	}

	public void setNumeroTitulo(Long numeroTitulo) {
		this.numeroTitulo = numeroTitulo;
	}

	public long getId() {
		return id;
	}
	public long getParcela() {
		return parcela;
	}
	public String getDescricao() {
		return descricao;
	}
	public Date getDataLancamento() {
		return dataLancamento;
	}
	public Date getDataEmissao() {
		return dataEmissao;
	}
	public Date getDataVencimento() {
		return dataVencimento;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public Character getSituacao() {
		return situacao;
	}
	public BigDecimal getJuro() {
		return juro;
	}
	public BigDecimal getDesconto() {
		return desconto;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setParcela(long parcela) {
		this.parcela = parcela;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}
	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	public void setSituacao(Character situacao) {
		this.situacao = situacao;
	}
	public void setJuro(BigDecimal juro) {
		this.juro = juro;
	}
	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}

	@Override
	public String toString() {
		return "Titulo [id=" + id + ", parcela=" + parcela + ", descricao="
				+ descricao + ", dataLancamento=" + dataLancamento
				+ ", dataEmissao=" + dataEmissao + ", dataVencimento="
				+ dataVencimento + ", valor=" + valor + ", saldo=" + saldo
				+ ", situacao=" + situacao + ", juro=" + juro + ", desconto="
				+ desconto + "]";
	}
}
