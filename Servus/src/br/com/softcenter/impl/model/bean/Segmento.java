package br.com.softcenter.impl.model.bean;


/**
 * @author Rodrigo Junior Utiyama
 * @since  09-09-2015
 */

public abstract class Segmento {
	private boolean estaEmConformidade = true;
	private boolean isDuplicado = false;
	private String nomeArquivo;
	
	public boolean isEstaEmConformidade() {
		return estaEmConformidade;
	}

	public void setEstaEmConformidade(boolean estaEmConformidade) {
		this.estaEmConformidade = estaEmConformidade;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public boolean isDuplicado() {
		return isDuplicado;
	}

	public void setDuplicado(boolean isDuplicado) {
		this.isDuplicado = isDuplicado;
	}
}
