package br.com.softcenter.impl.model.bean;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */
public class Cliente {
	private Long clienteID;
	private String razaoSocial;
	
	public Cliente(Long clienteID, String razaoSocial) {
		super();
		this.clienteID = clienteID;
		this.razaoSocial = razaoSocial;
	}
	
	public Long getClienteID() {
		return clienteID;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setClienteID(Long clienteID) {
		this.clienteID = clienteID;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	
}