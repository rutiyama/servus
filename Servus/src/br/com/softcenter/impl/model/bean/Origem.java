package br.com.softcenter.impl.model.bean;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */
public class Origem {
	private String descricao;
	private Long idOrg;
	
	public Origem(String descricao, Long idOrg) {
		this.descricao = descricao;
		this.idOrg = idOrg;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public Long getIdOrg() {
		return idOrg;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setIdOrg(Long idOrg) {
		this.idOrg = idOrg;
	}
}
