package br.com.softcenter.impl.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import br.com.softcenter.config.connection.ConnectionConfig;
import br.com.softcenter.impl.model.bean.ContaMovimento;
import br.com.softcenter.impl.model.bean.SegmentoT;
import br.com.softcenter.utils.database.DatabaseUtils;
import br.com.softcenter.utils.date.DateUtils;


/**
 * @author Rodrigo Junior Utiyama
 * @since  14-09-2015
 */
public class IDAOContaMovimento implements IDAOModel<ContaMovimento, Long>{

	private Connection conn;
	
	public IDAOContaMovimento() {
		conn = ConnectionConfig.getConn();
	}
	
	@Override
	public List<ContaMovimento> select() {
		return null;
	}

	@Override
	public ContaMovimento select(Long key) {
		return null;
	}

	@Override
	public ContaMovimento select(Long key, String where) {
		return null;
	}

	public void insert(ContaMovimento cm) {
		String sql = "INSERT INTO apr.contamovimento "
				   + "	(con_id, tib_id, ORG_ID, CMV_DATA, CMV_VALOR, CMV_DOCUMENTO, "
				   + "		CMV_HISTORICO, CMV_TIPO, CMV_TIPOMOVIMENTO, FPR_ID) " 
				   + "VALUES(?,?,?,?,?,?,?,?,?,?) ";
		
		PreparedStatement pstmt = null;
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, 18);
			pstmt.setLong(2, DatabaseUtils.RetornarUltimoValorDaTabela("titulobaixa", "TIB_ID"));
			pstmt.setLong(3, cm.getOrgID());
			pstmt.setString(4, DateUtils.ConvertToMySQLDateString(DateUtils.ConvertUtilToSQLDate(cm.getData())));
			pstmt.setBigDecimal(5, cm.getValorSacado());
			pstmt.setLong(6, cm.getDocumento());
			pstmt.setString(7, cm.getHistorico());
			pstmt.setString(8, cm.getTipo());
			pstmt.setString(9, "A");
			pstmt.setLong(10, Long.parseLong("6"));
			
			pstmt.executeUpdate();
			pstmt.close();
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}catch(NullPointerException npe){
			npe.printStackTrace();
			return;
		}
	}

	public void update(ContaMovimento segmento) {
		
	}

	@Override
	public void insert(SegmentoT segmento) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(SegmentoT segmento) {
		// TODO Auto-generated method stub
		
	}
}
