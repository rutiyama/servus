package br.com.softcenter.impl.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.softcenter.impl.model.bean.Segmento;
import br.com.softcenter.impl.model.bean.SegmentoT;
import br.com.softcenter.impl.model.bean.SegmentoU;
import br.com.softcenter.utils.alerta.MensagemTipo;
import br.com.softcenter.utils.alerta.ShowMessage;
import br.com.softcenter.utils.date.DateUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since  08-09-2015
 */

public class ArquivoRetornoController {
	
	private File arquivoRetorno;
	private FileReader fr;
	private BufferedReader bufferRead;
	private String urlArquivoRetorno;
	private SegmentoT segmentoT;
	private SegmentoU segmentoU;
	private Logger logger;
	
	public ArquivoRetornoController(String url){
		this.urlArquivoRetorno = url;
		logger = Logger.getLogger("br.com.softcenter.impl.controller");
	}
	
	public ArquivoRetornoController() {
		logger = Logger.getLogger("br.com.softcenter.impl.controller");
	}

	public List<Segmento> lerArquivoRetorno(){
		return lerArquivoRetorno(this.urlArquivoRetorno);
	}
	
	public List<Segmento> lerArquivoRetorno(String url){
		try{
			this.arquivoRetorno = new File(urlArquivoRetorno);
			this.fr = new FileReader(arquivoRetorno);
			this.bufferRead = new BufferedReader(fr);
			String linha = "";
			int cont = 0; 
			List<Segmento> segArr = new ArrayList<Segmento>();
			
			while((linha = this.bufferRead.readLine()) != null){
				++cont;
				
				if(cont > 2 && 
						(linha.subSequence(0, 3).equals("033") ||
						 linha.subSequence(0, 3).equals("353") ||
						 linha.subSequence(0, 3).equals("008") ) &&
						 (linha.substring(13,14).charAt(0) != ' ')  ){
					
					SegmentoT segT = popularArquivosRetorno(linha);
					
					if(segT.getCodigoMovimento().equals("06") || segT.getCodigoMovimento().equals("17")){
						if(cont % 2 == 0){
							segArr.add(popularArquivosRetorno(linha));
						}
//						else{
//							//popularArquivosRetorno(linha);
//						}
					}
				}
			}
			this.fr.close();
			this.bufferRead.close();
			
			return segArr;
		}catch(IOException ioex){
			ShowMessage.Exibir("Falha na leitura do arquivo. Verifique o caminho informado.", MensagemTipo.ERRO);
			ioex.printStackTrace();
			logger.debug("Erro na Leitura do retorno", ioex);
		}catch(ParseException pex){
			ShowMessage.Exibir("Falha na leitura do arquivo. Verifique se o seu conteúdo está íntegro.", MensagemTipo.ERRO);
			pex.printStackTrace();
			logger.debug("Erro na integridade do arquivo", pex);
		} 
		
		return null;
	}
	
	/*
	 * Este método é chamado em cada "Loop" do while, no método lerArquivoRetorno.
	 * É responsável por instanciar os atributos de Segmento, retornando um objeto 
	 * do tipo SegmentoT. Este objeto possui os dados do arquivo de retorno e prin-
	 * cipalmente um objeto do tipo SegmentoU (que é um COMPLEMENTO) do SegmentoT:
	 */
	private SegmentoT popularArquivosRetorno(String linha) throws ParseException{
		
		if(linha.substring(13,14).charAt(0) == 'U'){
			BigDecimal vlrPagoSacado= new BigDecimal(linha.substring(77,92));
			vlrPagoSacado = vlrPagoSacado.divide(new BigDecimal(100));
			
			BigDecimal vlrJurosMultaEncargos = new BigDecimal(linha.substring(17, 32));
			vlrJurosMultaEncargos = vlrJurosMultaEncargos.divide(new BigDecimal(100));
			
			BigDecimal vlrDescontoConcedido = new BigDecimal(linha.substring(32, 47));
			vlrDescontoConcedido = vlrDescontoConcedido.divide(new BigDecimal(100));
			
					
			this.segmentoU = new SegmentoU(vlrJurosMultaEncargos, vlrPagoSacado, vlrDescontoConcedido,
							  	DateUtils.ConvertStringToUtilDate(DateUtils.FormatDateStringToMySQL(linha.substring(145, 153))),
							  	DateUtils.ConvertStringToUtilDate(DateUtils.FormatDateStringToMySQL(linha.substring(137, 145)))
							  );  
		}
		
		if(linha.substring(13,14).charAt(0) == 'T'){
			this.segmentoT = new SegmentoT();
			
			if(linha.substring(100, 125).trim().isEmpty())
				this.segmentoT.setIdentificacaoNossoNumero(linha.substring(40, 52).replaceFirst("^0+(?!$)", "")); 
			else
				this.segmentoT.setIdentificadorBanco((linha.substring(100, 125)).replaceFirst("^0+(?!$)", ""));
			
			this.segmentoT.setNomeDoSacadoProperty(linha.substring(143, 183).trim());
			this.segmentoT.setCodigoMovimento(linha.substring(15,17));
			
			this.segmentoT.setIdentificacaoSeuNumero(linha.substring(54,69).replaceFirst("^0+(?!$)", "").trim());
			this.segmentoT.setDataVencimento(DateUtils.ConvertStringToUtilDate(DateUtils.FormatDateStringToMySQL(linha.substring(69, 77))));
			
			BigDecimal vlrTitulo = new BigDecimal(linha.substring(77, 92));
			vlrTitulo = vlrTitulo.divide(new BigDecimal(100));
			
			this.segmentoT.setValorDoTituloNominal(vlrTitulo);
		}
		
		if(this.segmentoU != null){
			this.segmentoT.setSegmentoU(segmentoU);
			this.segmentoU = null;
		}
		
		this.segmentoT.setNomeArquivo(arquivoRetorno.getName());
		return segmentoT;
	}
}