package br.com.softcenter.impl.controller;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;


public class SplashScreen {
	    public static final String APPLICATION_ICON =
	            "resources/carregando.gif";
	    public static final String SPLASH_IMAGE =
	            "resources/carregando.gif";

	    private Pane splashLayout;
	    private Label progressText;
	    private Stage mainStage;
	    private static final int SPLASH_WIDTH = 32;
	    private static final int SPLASH_HEIGHT = 32;
	    private Stage startStage;
	    private Stage stagePai;
	    private Paint corPadrao;
	    
	    public void init() {
	        ImageView splash = new ImageView(new Image(
	                getClass().getClassLoader().getResourceAsStream(SPLASH_IMAGE)
	        ));
	        //loadProgress = new ProgressBar();
	        //loadProgress.setPrefWidth(SPLASH_WIDTH - 20);
	        progressText = new Label("Carregando arquivos...");
	        splashLayout = new VBox();
	        splashLayout.getChildren().addAll(splash);
	        progressText.setAlignment(Pos.CENTER);
	        splashLayout.setStyle("-fx-background-color: rgba(255,255,255,0);");
//	        splashLayout.setEffect(new DropShadow());
	    }

	    public void start(final Stage initStage, Stage stagePai) throws Exception {
	    	this.stagePai = stagePai;
	        showSplash(
	                initStage, 
	                () -> showMainStage()
	        );
	        new Thread().start();
	    }

	    private void showMainStage() {
	        mainStage = new Stage(StageStyle.TRANSPARENT);
	        mainStage.setWidth(SPLASH_WIDTH);
	        mainStage.setHeight(SPLASH_HEIGHT);
	        mainStage.getIcons().add(new Image(
	                getClass().getClassLoader().getResourceAsStream(APPLICATION_ICON)
	        ));
	        
//	        mainStage.show();
	    }
	    private void showSplash(
	            final Stage initStage,
	            InitCompletionHandler initCompletionHandler
	    ) {
	    	initStage.setWidth(SPLASH_WIDTH);
	    	initStage.setHeight(SPLASH_HEIGHT);
            initStage.toFront();
            this.startStage = initStage;

            FadeTransition fadeSplash = new FadeTransition(Duration.INDEFINITE, splashLayout);
            
            fadeSplash.setFromValue(1.0);
            fadeSplash.setToValue(0.0);
            fadeSplash.setOnFinished(actionEvent -> initStage.hide());
            fadeSplash.play();

            initCompletionHandler.complete();

	        Scene splashScene = new Scene(splashLayout);
	        splashScene.setFill(Color.TRANSPARENT);
	        initStage.initStyle(StageStyle.TRANSPARENT);
	        final Rectangle2D bounds = Screen.getPrimary().getBounds();
	        
	        initStage.setScene(splashScene);
	        initStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
	        initStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
	        initStage.initModality(Modality.APPLICATION_MODAL);
	        initStage.show(); 
	    }

	    public interface InitCompletionHandler {
	        public void complete();
	    }
	    
	    public void end(){
	    	Platform.runLater(new Runnable() {
				@Override
				public void run() {
					startStage.close();					
				}
			});
	    }
	}
