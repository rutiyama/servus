package br.com.softcenter.impl.controller;

import java.math.BigDecimal;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import br.com.softcenter.impl.model.PopularGridSegmentoStrategy;
import br.com.softcenter.impl.model.bean.GridFaturasJavaFXBean;
import br.com.softcenter.impl.model.bean.SegmentoT;
import br.com.softcenter.utils.date.DateUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since  09-09-2015
 */

public class PopularGridRetornoSegmentoU implements PopularGridSegmentoStrategy{

	@Override
	public void popularGridRetornoJavaFx(GridFaturasJavaFXBean gridFaturasBean, List<String>  duplicados) {
		//Coluna Data Credito
//		gridFaturasBean.getColDataEfetivacaoCredito().
//			setCellValueFactory(cellData -> {
//				String dataStr = DateUtils.ConvertDateUtilToString(
//						((SegmentoT)cellData.getValue()).getSegmentoU().getDataEfetivacaoCreditoProperty().get());
//				return new SimpleStringProperty(dataStr);
//			});
		
		gridFaturasBean.getColDataEfetivacaoCredito().
		setCellValueFactory(cellData -> {
			String dataStr = DateUtils.ConvertDateUtilToString(
					((SegmentoT)cellData.getValue()).getSegmentoU().getDataLiquidacao().get());
			return new SimpleStringProperty(dataStr);
		});

		gridFaturasBean.getColValorDiferenca().
			setCellValueFactory(cellData -> {
				BigDecimal bd = ((SegmentoT)cellData.getValue()).getSegmentoU().getJurosMultasEncargosProperty().get().setScale(2);
				((SegmentoT)cellData.getValue()).getSegmentoU().getJurosMultasEncargosProperty().set(bd);
				return ((SegmentoT)cellData.getValue()).getSegmentoU().getJurosMultasEncargosProperty();
		});
		
		gridFaturasBean.getColValorPagoSacado().
			setCellValueFactory(cellData -> {
				try {
					BigDecimal bd = ((SegmentoT)cellData.getValue()).getSegmentoU().getValorPagoSacadoProperty().get().setScale(2);
					((SegmentoT)cellData.getValue()).getSegmentoU().getValorPagoSacadoProperty().set(bd);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return ((SegmentoT)cellData.getValue()).getSegmentoU().getValorPagoSacadoProperty();
		});
		
	}
}

	