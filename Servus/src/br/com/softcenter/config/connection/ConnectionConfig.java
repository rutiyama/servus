package br.com.softcenter.config.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import javafx.beans.property.Property;
import br.com.softcenter.utils.ini.IniUtils;
import br.com.softcenter.utils.ini.IniUtilsImpl;

/**
 * @author Rodrigo Junior Utiyama
 * @since  03-09-2015
 */


public class ConnectionConfig {
	private static Connection conn;
	private static ConnectionConfig connectionConfig;
	private Logger logger;
	
	private ConnectionConfig(){
		startConnection();
	}
	
	public static Connection getConn(){
		if(conn == null){
			connectionConfig = new ConnectionConfig();
			try{
				conn.setAutoCommit(false);
			}catch(SQLException sqlex){
				sqlex.printStackTrace();
			}	
		}
		
		return connectionConfig.conn;
	}

	public static void closeConn(){
		try{
			if(conn != null)
				closeConnection();
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}
	
	private void startConnection(){
		IniUtils ini = new IniUtilsImpl();
		logger = Logger.getLogger("br.com.softcenter.config.connection");
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(ini.selectIni().getDatabase_url()  + "user=" 	 +
											   ini.selectIni().getDatabase_user() + "&password=" +
											   ini.selectIni().getDatabase_pass());
		}catch(ClassNotFoundException  classnfe){
			logger.debug("Objeto do Banco Não encontrado", classnfe);
			classnfe.printStackTrace();
			
		}catch(SQLException sqle){
			logger.debug("Erro ao conectar", sqle);
		}
	}
	
	
	private static void closeConnection() throws SQLException{
		if (conn != null){
			conn.close();
		}
	}
}
