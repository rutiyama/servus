package br.com.softcenter.utils.alerta;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */
public class MensagemAlerta implements InterfaceCaixaMensagem{

	@Override
	public void Exibir(String mensagem) {
		
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				Alert alerta = new Alert(AlertType.WARNING);
				alerta.setTitle("Alerta");
				alerta.setHeaderText("Alerta");
				alerta.setContentText(mensagem);
				alerta.showAndWait();		
			}
		});
	}

}
