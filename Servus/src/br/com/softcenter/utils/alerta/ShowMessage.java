package br.com.softcenter.utils.alerta;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */
public class ShowMessage {
	public static void Exibir(String mensagem, MensagemTipo tipo){
		InterfaceCaixaMensagem factory = null;
		
		switch(tipo){
			case INFORMACAO:  factory = new MensagemInformacao();  break;
			case ALERTA:  	  factory = new MensagemAlerta();      break;
			case ERRO:  	  factory = new MensagemErro();		  break;
			default: 	      factory = new MensagemInformacao();
		}
		
		factory.Exibir(mensagem);
	}
}
