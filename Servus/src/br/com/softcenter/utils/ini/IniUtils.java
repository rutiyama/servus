package br.com.softcenter.utils.ini;

import br.com.softcenter.impl.model.bean.BeanIniUtils;


/**
 * @author Rodrigo Junior Utiyama
 * @since  03-09-2015
 */

public interface IniUtils {
	
	//Retorna uma instancia contendo os dados do arquivo INI:
	public BeanIniUtils selectIni();
}
