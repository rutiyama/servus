package br.com.softcenter.utils.database;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import br.com.softcenter.config.connection.ConnectionConfig;
import br.com.softcenter.utils.date.DateUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since  11-09-2015
 */

/**
 * Classe responsável por realizar operações básicas 
 * do banco de dados. Tais como consultas de FK ou PK...
 */
public class DatabaseUtils {
	
	private static String sql;
	
	public static boolean VerificarSeTituloJaFoiImportado(Date data, String nossonumero){
		sql = "SELECT tib_id FROM apr.titulobaixa " +  
			  "		INNER JOIN apr.titulo ON (apr.titulo.tit_id = apr.titulobaixa.tit_id) " + 
			  " 	INNER JOIN apr.boleto ON (apr.titulo.tit_id = apr.boleto.tit_id) 	  " + 
			  "WHERE  Date(apr.titulobaixa.tib_data) = ? and  apr.boleto.BLT_NOSSONUMERO = ?";
		
		try{
			PreparedStatement pstmt = ConnectionConfig.getConn().prepareStatement(sql);
			pstmt.setDate(1, DateUtils.ConvertUtilToSQLDate(data));
			pstmt.setString(2, nossonumero);
				
			if(pstmt.executeQuery().next())
				return true;
			
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		return false;
	}
	
	public static Long RetornarIdTituloPorNossoNumero(Long nossoNumero){
		//sql = "SELECT tit_id FROM apr.boleto WHERE apr.boleto.BLT_NOSSONUMERO = ? AND apr.boleto.CCV_ID = ?";
		sql = "	SELECT tit_id FROM apr.boleto " + 
			  "		INNER JOIN apr.contaconvenio ON apr.contaconvenio.CCV_ID = apr.boleto.CCV_ID " +
		  	  "		INNER JOIN apr.conta ON (apr.contaconvenio.CON_ID = apr.conta.CON_ID) " +
			  "	WHERE apr.boleto.BLT_NOSSONUMERO = ? and apr.conta.CON_ID = ?";
		
		try{
			PreparedStatement pstmt = ConnectionConfig.getConn().prepareStatement(sql);
			pstmt.setLong(1, nossoNumero);
			pstmt.setInt(2, 18);
			
			ResultSet rs = pstmt.executeQuery();
		
			if(rs.next())
				return rs.getLong("TIT_ID");
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		
		return -1L;
	}
	
	public static Long RetornarIdTituloPorIdentificaoBanco(String titnumero){
		sql = "SELECT tit_id FROM apr.titulo WHERE apr.titulo.tit_numero = ?";
		
		if(titnumero.equals(""))
			return -1L;
		
		try{
			PreparedStatement pstmt = ConnectionConfig.getConn().prepareStatement(sql);
			pstmt.setString(1, titnumero);
			ResultSet rs = pstmt.executeQuery();
		
			if(rs.next())
				return rs.getLong("TIT_ID");
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		
		return -1L;
	}
	
	public static BigDecimal RetornarCalculoSaldoTitulo(Long tituloID, Long tituloBaixaId){
		sql = "SELECT ((apr.titulo.TIT_SALDO - apr.titulobaixa.TIB_VALOR) + " 
			  + " (apr.titulobaixa.TIB_JURO + apr.titulobaixa.TIB_MULTA - apr.titulobaixa.TIB_DESCONTO)) " 
			  + " AS SALDO FROM apr.titulo LEFT JOIN apr.titulobaixa "
			  + "	ON (apr.titulo.TIT_ID = apr.titulobaixa.TIT_ID) "
			  + " WHERE apr.titulo.TIT_ID = ? " 
			  + " and apr.titulobaixa.TIB_ID = ? ";
		
		try{
			PreparedStatement pstmt = ConnectionConfig.getConn().prepareStatement(sql);
			pstmt.setLong(1, tituloID);
			pstmt.setLong(2, tituloBaixaId);
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next())
				return new BigDecimal(rs.getString("SALDO"));
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		
		return new BigDecimal(-1);
	}
	
	public static Long RetornarUltimoValorDaTabela(String tabela, String campo){
		sql = "SELECT MAX("+campo+") AS ULTIMO_INSERT FROM " + tabela;
		try{
			PreparedStatement pstmt = ConnectionConfig.getConn().prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()){
				return rs.getLong("ULTIMO_INSERT");
			}
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		
		return new Long(-1);
	}
	
	public static Date RetornarDataLimiteLancamentoFilial(){
		sql = "SELECT FLL_DATALIMITELANCAMENTO FROM apr.filial WHERE fll_id = 1";
		
		PreparedStatement pstmt = null;
		try{
			pstmt = ConnectionConfig.getConn().prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()){
				return rs.getDate("FLL_DATALIMITELANCAMENTO");
			}
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		
		return null;
	}
	
	public static String RetornarSituacaoDoTitulo(Long nossoNumero){
		sql = "SELECT tit_situacao FROM apr.titulo WHERE tit_id = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String result = null;
		
		try{
			pstmt = ConnectionConfig.getConn().prepareStatement(sql);
			pstmt.setLong(1, RetornarIdTituloPorNossoNumero(nossoNumero));
			rs = pstmt.executeQuery();
			
			if(rs.next())
				result = rs.getString("tit_situacao");
			
			pstmt.close();
			rs.close();
			
			return result;
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		return null;
	}
	
	public static boolean AtualizarTabelaValoresAFaturar(Long idTitulo) throws SQLException{
		sql = "UPDATE apr.valoresafaturar SET vft_situacao = ? WHERE tit_id = ?";
		
		PreparedStatement pstmt = ConnectionConfig.getConn().prepareStatement(sql);
		pstmt.setString(1, "Q");
		pstmt.setLong(2, idTitulo);

		return pstmt.execute();
	}
	
	public static Long VerificarSeValorAFaturarExiste(Long idTitulo){
		sql = "SELECT apr.vft_id FROM apr.valoresafaturar WHERE tit_id = ? ";
		return -1L;
	}
	
}
