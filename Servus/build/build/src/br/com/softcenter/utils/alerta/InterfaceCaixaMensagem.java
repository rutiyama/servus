package br.com.softcenter.utils.alerta;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */
public interface InterfaceCaixaMensagem {
	public void Exibir(String mensagem);
}
