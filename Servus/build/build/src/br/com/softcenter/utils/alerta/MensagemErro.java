package br.com.softcenter.utils.alerta;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */
public class MensagemErro implements InterfaceCaixaMensagem{

	@Override
	public void Exibir(String mensagem) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Alert alerta = new Alert(AlertType.ERROR);
				alerta.setTitle("Mensagem de Erro");
				alerta.setHeaderText("Erro");
				alerta.setContentText(mensagem);
				alerta.showAndWait();	
			}
		});
	}

}
