package br.com.softcenter.utils.window;

import java.io.File;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * @author Rodrigo Junior Utiyama
 * @since  08-09-2015
 */
public class DiretorioUtils {
	public static String selectFile(String descricaoExtensao, Stage stage, String ... extensoes){
		FileChooser fileChooser = new FileChooser();
		FileChooser.ExtensionFilter extensions = new FileChooser.ExtensionFilter(descricaoExtensao, extensoes);
		fileChooser.getExtensionFilters().add(extensions);
		fileChooser.setTitle("Selecione o arquivo de Retorno");
		
		File arquivo = fileChooser.showOpenDialog(stage);
		
		if(arquivo != null)
			return arquivo.getAbsolutePath();
		
		return "";
	}
}
