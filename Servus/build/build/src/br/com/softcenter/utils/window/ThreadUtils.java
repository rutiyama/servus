package br.com.softcenter.utils.window;

import javafx.application.Platform;
import javafx.scene.control.Label;

public class ThreadUtils {
	
	public static void ExibirMensagemErroThread(Label textfield, String mensagem){
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				textfield.setText(mensagem);
			}
		});
	}
}
