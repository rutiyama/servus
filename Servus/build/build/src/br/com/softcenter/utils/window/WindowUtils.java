package br.com.softcenter.utils.window;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class WindowUtils {
	public static boolean showFXML(Class classe, Stage stage, String FileFXML, Initializable controller){
		return showFXML(classe, stage, FileFXML, "", controller);
	}
	
	public static boolean showFXML(Class classe, Stage stage, String FileFXML, String title, Initializable controller){
		
		try{
			FXMLLoader loader = new FXMLLoader(classe.getResource(FileFXML));
			loader.setController(controller);
			AnchorPane root = loader.load();

			Scene scene = new Scene(root);
	        Stage stageTemp = new Stage();
	        stageTemp.setTitle(title);
	        stageTemp.setScene(scene);
	        stageTemp.initModality(Modality.WINDOW_MODAL);
	        stageTemp.initOwner(stage.getScene().getWindow());
	        stageTemp.showAndWait();
	        
			return true;
		}catch(IOException ioex){
			ioex.printStackTrace();
		}
		
		return false;
	}
}
