package br.com.softcenter;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import br.com.softcenter.impl.controller.PrincipalController;

public class Principal extends Application {

	private Stage primaryStage;
	private AnchorPane rootLayout;
	
	@Override
	public void start(Stage stage) {
		this.primaryStage = stage;
        this.primaryStage.setTitle("SOFTCENTER - Retorno de Cobrança Bancária");

        
        try {
            FXMLLoader loader = new FXMLLoader(Principal.class.getResource("impl/view/Principal.fxml"));
            loader.setController(new  PrincipalController(stage));
            rootLayout = loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setResizable(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public static void main(String[] args) {
		launch(args);
	}
}
