package br.com.softcenter.impl.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.softcenter.config.connection.ConnectionConfig;
import br.com.softcenter.impl.model.bean.ContaSaldoDia;
import br.com.softcenter.impl.model.bean.SegmentoT;
import br.com.softcenter.utils.date.DateUtils;
 
/**
 * @author Rodrigo Junior Utiyama
 * @since  03-09-2015
 */

public class IDAOContaSaldoDia implements IDAOModel<ContaSaldoDia, Long>{
	
	private Connection conn;
	private PreparedStatement pstmt;
	private Date dataParaConsulta;
	
	public IDAOContaSaldoDia(){
		conn = ConnectionConfig.getConn();
	}

	@Override
	public List<ContaSaldoDia> select() {
		return null;
	}

	@Override
	public ContaSaldoDia select(Long key) throws SQLException {
		String sql = "SELECT * FROM apr.contasaldodia WHERE contasaldodia.csd_data = ? AND con_id = ? ";
		
		pstmt = conn.prepareStatement(sql);
		pstmt.setDate(1, DateUtils.ConvertUtilToSQLDate(dataParaConsulta));
		pstmt.setLong(2, key);
		
		ResultSet rs = pstmt.executeQuery();

		if(rs.next()){
			ContaSaldoDia contaSD =  new ContaSaldoDia();
			contaSD.setContaSDID(rs.getLong("CSD_ID"));
			contaSD.setContaID(rs.getLong("CON_ID"));
			contaSD.setData(rs.getDate("CSD_DATA"));
			contaSD.setSaldo(rs.getBigDecimal("CSD_SALDO"));
			contaSD.setSaldoAcumulado(rs.getBigDecimal("CSD_SALDOACUMULADO"));
			contaSD.setTotalCredito(rs.getBigDecimal("CSD_TOTALCREDITO"));
			contaSD.setTotalDebito(rs.getBigDecimal("CSD_TOTALDEBITO"));
			
			return contaSD;
		}
		
		return null;
	}

	@Override
	public ContaSaldoDia select(Long key, String where) {
		return null;
	}

	@Override
	public void insert(SegmentoT segmento) { }

	@Override
	public void update(SegmentoT segmento) { }
	
	public void insert(ContaSaldoDia contaSaldoDia){
		String sql =  "  INSERT INTO apr.contasaldodia (CON_ID, CSD_DATA, CSD_TOTALDEBITO, "
					+ " CSD_TOTALCREDITO, CSD_SALDO, CSD_SALDOACUMULADO) VALUES(?,?,?,?,?,?) ";
		
		PreparedStatement pstmt = null;
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, contaSaldoDia.getContaID());
			pstmt.setDate(2, DateUtils.ConvertUtilToSQLDate(contaSaldoDia.getData()));
			pstmt.setBigDecimal(3, contaSaldoDia.getTotalDebito());
			pstmt.setBigDecimal(4, contaSaldoDia.getTotalCredito());
			pstmt.setBigDecimal(5, contaSaldoDia.getSaldo());
			pstmt.setBigDecimal(6, contaSaldoDia.getSaldoAcumulado());
			pstmt.executeUpdate();
			pstmt.close();
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
	}
	
	public void update(ContaSaldoDia contaSaldoDia){
		String sql = "UPDATE apr.contasaldodia SET "
						+ " CON_ID = ?, CSD_DATA = ?, CSD_TOTALDEBITO = ?, "
						+ " CSD_TOTALCREDITO = ?, CSD_SALDO = ?, csd_saldoacumulado = ?  "
						+ " WHERE CSD_ID = ?";
		
		PreparedStatement pstmt = null;
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, contaSaldoDia.getContaID());
			pstmt.setDate(2, DateUtils.ConvertUtilToSQLDate(contaSaldoDia.getData()));
			pstmt.setBigDecimal(3, contaSaldoDia.getTotalDebito());
			pstmt.setBigDecimal(4, contaSaldoDia.getTotalCredito());
			pstmt.setBigDecimal(5, contaSaldoDia.getSaldo());
			pstmt.setBigDecimal(6, contaSaldoDia.getSaldoAcumulado());
			pstmt.setLong(7, contaSaldoDia.getContaSDID());
			pstmt.executeUpdate();
			pstmt.close();
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
	}
	
	public ContaSaldoDia select(Date dataParaConsulta, Long key) throws SQLException{
		this.dataParaConsulta = dataParaConsulta;
		return select(key);
	}
	
	public List<ContaSaldoDia> selectAll(Date data,Long key, TipoSelecao tipo) throws SQLException{
		String sql;
		
		if(tipo == TipoSelecao.DATA_ANTERIOR)
			sql = "SELECT * FROM apr.contasaldodia WHERE contasaldodia.csd_data < ? AND con_id = ? ";
		else
			sql = "SELECT * FROM apr.contasaldodia WHERE contasaldodia.csd_data > ? AND con_id = ? ";
		
		pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, DateUtils.ConvertToMySQLDateString(DateUtils.ConvertUtilToSQLDate(data)));
		pstmt.setLong(2, key);
		ResultSet rs = pstmt.executeQuery();
		List<ContaSaldoDia> csdList = new ArrayList<ContaSaldoDia>();
		
		while(rs.next()){
			ContaSaldoDia contaSD =  new ContaSaldoDia();
			contaSD.setContaSDID(rs.getLong("CSD_ID"));
			contaSD.setContaID(rs.getLong("CON_ID"));
			contaSD.setData(rs.getDate("CSD_DATA"));
			contaSD.setSaldo(rs.getBigDecimal("CSD_SALDO"));
			contaSD.setSaldoAcumulado(rs.getBigDecimal("CSD_SALDOACUMULADO"));
			contaSD.setTotalCredito(rs.getBigDecimal("CSD_TOTALCREDITO"));
			contaSD.setTotalDebito(rs.getBigDecimal("CSD_TOTALDEBITO"));
			csdList.add(contaSD);
		}
		
		return csdList;
	}
	
	public ContaSaldoDia selectLimitOne(Date data, Long key, TipoSelecao tipo) throws SQLException{
		String sql =   "SELECT  * FROM apr.contasaldodia WHERE contasaldodia.csd_data < ? AND con_id = ? "
					 + " ORDER BY  contasaldodia.CSD_DATA DESC LIMIT 1";
		
		PreparedStatement pstmt = null;
		
		pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, DateUtils.ConvertToMySQLDateString(DateUtils.ConvertUtilToSQLDate(data)));
		pstmt.setLong(2, key);
		ResultSet rs = pstmt.executeQuery();
		
		if(rs.next()){
			ContaSaldoDia contaSD =  new ContaSaldoDia();
			contaSD.setContaSDID(rs.getLong("CSD_ID"));
			contaSD.setContaID(rs.getLong("CON_ID"));
			contaSD.setData(rs.getDate("CSD_DATA"));
			contaSD.setSaldo(rs.getBigDecimal("CSD_SALDO"));
			contaSD.setSaldoAcumulado(rs.getBigDecimal("CSD_SALDOACUMULADO"));
			contaSD.setTotalCredito(rs.getBigDecimal("CSD_TOTALCREDITO"));
			contaSD.setTotalDebito(rs.getBigDecimal("CSD_TOTALDEBITO"));
			
			return contaSD;
		}
		
		return null;
	}
	
	public void setDataConsulta(Date dataConsulta){
		this.dataParaConsulta = dataConsulta;
	}
}
