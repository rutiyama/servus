package br.com.softcenter.impl.model;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softcenter.config.connection.ConnectionConfig;
import br.com.softcenter.impl.model.bean.SegmentoT;
import br.com.softcenter.impl.model.bean.TituloBaixa;
import br.com.softcenter.utils.database.DatabaseUtils;
import br.com.softcenter.utils.date.DateUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since  10-09-2015
 */
public class IDAOTituloBaixaImpl implements IDAOModel<TituloBaixa, Integer> {

	private Connection conn = null;
	
	public IDAOTituloBaixaImpl() {
		conn = ConnectionConfig.getConn();
	}
	
	//Faz a seleção com a chave primária:
	@Override
	public List<TituloBaixa> select(){
		return selectAllTitulo("TIB_ID");
	}
 
	@Override
	public TituloBaixa select(Integer key) throws SQLException {
		String sql = "SELECT * FROM titulobaixa WHERE tib_id = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, key);
			
			rs = pstmt.executeQuery();
			while(rs.next()){
				if(rs.getLong("TIT_ID") == key){
					return new TituloBaixa(
							rs.getLong("TIT_ID"),
							DateUtils.ConvertStringToUtilDate(rs.getString("TIB_DATA")), 
							new BigDecimal(rs.getString("TIB_VALOR")),
							new BigDecimal(rs.getString("TIB_JURO")),
							new BigDecimal(rs.getString("TIB_DESCONTO")),
							rs.getString("TIB_TIPO").toCharArray()[0]);
				}
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}catch(ParseException parseEx){
			parseEx.printStackTrace();
		}finally{
			pstmt.close();
			rs.close();
		}
		
		return null;
	}

	@Override
	public TituloBaixa select(Integer key, String where) {
		String sql = "SELECT * FROM apr.titulobaixa WHERE "+where+" = ?";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, key);
			rs = pstmt.executeQuery();
			if(rs.next()){
				TituloBaixa titulo = new TituloBaixa(
						rs.getLong("TIB_ID"),
						DateUtils.ConvertStringToUtilDate(rs.getString("TIB_DATA")), 
						new BigDecimal(rs.getString("TIB_VALOR")),
						new BigDecimal(rs.getString("TIB_JURO")),
						new BigDecimal(rs.getString("TIB_DESCONTO")),
						rs.getString("TIB_TIPO").toCharArray()[0]);
				
				pstmt.close();
				rs.close();
				
				return titulo;
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}catch(ParseException parseEx){
			parseEx.printStackTrace();
		}  
		return null;
	}

	private List<TituloBaixa> selectAllTitulo(String campo){
		String sql = "SELECT * FROM titulobaixa";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<TituloBaixa> titulosBaixa = null;
		
		try{
			conn = ConnectionConfig.getConn();
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			titulosBaixa = new ArrayList<TituloBaixa>();
			
			while(rs.next()){
				titulosBaixa.add(select(rs.getInt(campo), campo));
			}
			
			pstmt.close();
			rs.close();	
		}catch(SQLException ioex){
			ioex.printStackTrace();
		}
		return titulosBaixa;
	}
	
	@Override
	public void insert(SegmentoT segmento) {
		String sql = " INSERT INTO apr.titulobaixa (tit_id, tib_data, tib_valor,tib_juro, " + 
				 " tib_desconto, tib_multa, tib_tipo, fpr_id, tib_observacao ) 		  " + 
				 " VALUES (?,?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		try{
			pstmt = conn.prepareStatement(sql);	
			String dataEfetivacaoCredito = DateUtils.ConvertToMySQLDateString(segmento.getSegmentoU().getDataEfetivacaoCredito());
			//Long l = DatabaseUtils.RetornarIdTituloPorNossoNumero(Long.parseLong(segmento.getIdentificacaoNossoNumero()));
			
			//Exibir Mensagem
//			if(l == -1)
//				return;
			
			if(segmento.getTitID() == -1)
				return;
						
			pstmt.setLong(1, segmento.getTitID());
			pstmt.setDate(2, DateUtils.ConvertUtilToSQLDate(DateUtils.ConvertStringToUtilDate(dataEfetivacaoCredito)));
			pstmt.setBigDecimal(3, segmento.getSegmentoU().getValorPagoSacado().subtract(segmento.getSegmentoU().getJurosMultasEncargos()));
			pstmt.setBigDecimal(4, segmento.getSegmentoU().getJurosMultasEncargos());
			pstmt.setBigDecimal(5, segmento.getSegmentoU().getValorDesconto());
			pstmt.setDouble(6, 0.00);
			pstmt.setString(7, "T");
			pstmt.setInt(8, 6);
			pstmt.setString(9, "Liquidação efetuada através do retorno do arquivo (Soft) -  " + segmento.getNomeArquivo());
			pstmt.executeUpdate();
			
			pstmt.close();
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}catch(ParseException pex){
			pex.printStackTrace();
		}		
	}

	@Override
	public void update(SegmentoT segmento) {
		String sql = "UPDATE apr.titulobaixa SET tit_id =?, tib_data=?, tib_valor = ?, tib_juro = ?, " + 
				 	 " tib_desconto = ?, tib_multa = ?, tib_tipo = ?, fpr_id = ?, tib_observacao = ? " +
				 	 " WHERE tib_id = ?";
		
		PreparedStatement pstmt = null;
		
		try{
			pstmt = conn.prepareStatement(sql);	
			String dataEfetivacaoCredito = DateUtils.ConvertToMySQLDateString(segmento.getSegmentoU().getDataEfetivacaoCredito());
			Long ultimoRegistro = DatabaseUtils.RetornarUltimoValorDaTabela("titulobaixa", "TIB_ID");
//			Long l = DatabaseUtils.RetornarIdTituloPorNossoNumero(Long.parseLong(segmento.getIdentificacaoNossoNumero()));
			String tipo = null;
			
			if(segmento.getTipo().equals("Q"))
				tipo = "T";
			else
				tipo = "P";
			
			pstmt.setLong(1, segmento.getTitID());
			pstmt.setDate(2, DateUtils.ConvertUtilToSQLDate(DateUtils.ConvertStringToUtilDate(dataEfetivacaoCredito)));
			pstmt.setBigDecimal(3, segmento.getSegmentoU().getValorPagoSacado().subtract(segmento.getSegmentoU().getJurosMultasEncargos())); //Alteracao 1
			pstmt.setBigDecimal(4, segmento.getSegmentoU().getJurosMultasEncargos());
			pstmt.setBigDecimal(5, segmento.getSegmentoU().getValorDesconto());
			pstmt.setDouble(6, 0.00);
			pstmt.setString(7, tipo);
			pstmt.setInt(8, 6);
			pstmt.setString(9, "Liquidação efetuada através do retorno do arquivo (Soft) -  " + segmento.getNomeArquivo());
			pstmt.setLong(10, ultimoRegistro);
			pstmt.executeUpdate();
			
			pstmt.close();
			
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}catch(ParseException pex){
			pex.printStackTrace();
		}	
		 
	}	
}
