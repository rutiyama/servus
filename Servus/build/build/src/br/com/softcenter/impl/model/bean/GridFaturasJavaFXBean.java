package br.com.softcenter.impl.model.bean;

import java.math.BigDecimal;
import java.util.Date;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;

/**
 * @author Rodrigo Junior Utiyama
 * @since  10-09-2015
 */

public class GridFaturasJavaFXBean extends GridFaturasJavaFX{
	private TableView<Segmento> gridFaturas;
	private TableColumn<Segmento, String> colSegmento, colTituloEmpresa, colTituloBanco,colSeuNro, colVencimento;
	private TableColumn<Segmento, String> colDataEfetivacaoCredito;
	private TableColumn<Segmento, BigDecimal> colValorDiferenca, colValorPagoSacado, colValorTituloNominal;
	private TableView<Segmento> table;
	private TableColumn<Segmento, String> colNCObservacao, colFornecedor, colNCSeuNro;
	private TableColumn<Segmento, ImageView> colSituacao;
	
	
	public GridFaturasJavaFXBean(TableView<Segmento> gridFaturas,
			TableColumn<Segmento, String> colSegmento,
			TableColumn<Segmento, String> colTituloEmpresa,
			TableColumn<Segmento, String> colDataEfetivacaoCredito,
			TableColumn<Segmento, String> colTituloBanco,
			TableColumn<Segmento, BigDecimal> colValorDiferenca,
			TableColumn<Segmento, BigDecimal> colValorPagoSacado,
			TableView<Segmento> table,
			TableColumn<Segmento, ImageView> colSituacao, TableColumn<Segmento, String> colFornecedor,
			TableColumn<Segmento, String> colSeuNro,
			TableColumn<Segmento, String> colVencimento,
			TableColumn<Segmento, BigDecimal> colValorTituloNominal){
		super();
		this.gridFaturas = gridFaturas;
		this.colSegmento = colSegmento;
		this.colTituloEmpresa = colTituloEmpresa;
		this.colDataEfetivacaoCredito = colDataEfetivacaoCredito;
		this.colTituloBanco = colTituloBanco;
		this.colValorDiferenca = colValorDiferenca;
		this.colValorPagoSacado = colValorPagoSacado;
		this.table = table;
		this.colSituacao = colSituacao;
		this.colFornecedor = colFornecedor;
		this.colSeuNro = colSeuNro;
		this.colVencimento = colVencimento;
		this.colValorTituloNominal = colValorTituloNominal;
	} 
	
	public GridFaturasJavaFXBean(TableView<Segmento> gridFaturas,
			TableColumn<Segmento, String> colSegmento,
			TableColumn<Segmento, String> colTituloEmpresa,
			TableColumn<Segmento, String> colDataEfetivacaoCredito,
			TableColumn<Segmento, String> colTituloBanco,
			TableColumn<Segmento, BigDecimal> colValorDiferenca,
			TableColumn<Segmento, BigDecimal> colValorPagoSacado,
			TableView<Segmento> table, 
			TableColumn<Segmento, String> colNCObservacao,
			TableColumn<Segmento, ImageView> colSituacao, 
			TableColumn<Segmento, String> colFornecedor,
			TableColumn<Segmento, String> colNCSeuNro) {
		super();
		this.gridFaturas = gridFaturas;
		this.colSegmento = colSegmento;
		this.colTituloEmpresa = colTituloEmpresa;
		this.colDataEfetivacaoCredito = colDataEfetivacaoCredito;
		this.colTituloBanco = colTituloBanco;
		this.colValorDiferenca = colValorDiferenca;
		this.colValorPagoSacado = colValorPagoSacado;
		this.table = table;
		this.colNCObservacao = colNCObservacao;
		this.colSituacao = colSituacao;
		this.colFornecedor = colFornecedor;
		this.colSeuNro = colNCSeuNro;
	}
	
	
	public TableColumn<Segmento, ImageView> getColSituacao() {
		return colSituacao;
	}

	public void setColSituacao(TableColumn<Segmento, ImageView> colSituacao) {
		this.colSituacao = colSituacao;
	}

	public TableView<Segmento> getGridFaturas() {
		return gridFaturas;
	}
	public TableColumn<Segmento, String> getColSegmento() {
		return colSegmento;
	}
	public TableColumn<Segmento, String> getColTituloEmpresa() {
		return colTituloEmpresa;
	}
	public TableColumn<Segmento, String> getColDataEfetivacaoCredito() {
		return colDataEfetivacaoCredito;
	}
	public TableColumn<Segmento, String> getColTituloBanco() {
		return colTituloBanco;
	}
	public TableColumn<Segmento, BigDecimal> getColValorDiferenca() {
		return colValorDiferenca;
	}
	public TableColumn<Segmento, BigDecimal> getColValorPagoSacado() {
		return colValorPagoSacado;
	}
	public TableView<Segmento> getTable() {
		return table;
	}
	public TableColumn<Segmento, String> getColNCObservacao() {
		return colNCObservacao;
	}
	public void setGridFaturas(TableView<Segmento> gridFaturas) {
		this.gridFaturas = gridFaturas;
	}
	public void setColSegmento(TableColumn<Segmento, String> colSegmento) {
		this.colSegmento = colSegmento;
	}
	public void setColTituloEmpresa(TableColumn<Segmento, String> colTituloEmpresa) {
		this.colTituloEmpresa = colTituloEmpresa;
	}
	public void setColDataEfetivacaoCredito(
			TableColumn<Segmento, String> colDataEfetivacaoCredito) {
		this.colDataEfetivacaoCredito = colDataEfetivacaoCredito;
	}
	public void setColTituloBanco(TableColumn<Segmento, String> colTituloBanco) {
		this.colTituloBanco = colTituloBanco;
	}
	public void setColValorDiferenca(
			TableColumn<Segmento, BigDecimal> colValorDiferenca) {
		this.colValorDiferenca = colValorDiferenca;
	}
	public void setColValorPagoSacado(
			TableColumn<Segmento, BigDecimal> colValorPagoSacado) {
		this.colValorPagoSacado = colValorPagoSacado;
	}
	public void setTable(TableView<Segmento> table) {
		this.table = table;
	}

	public void setColNCObservacao(TableColumn<Segmento, String> colNCObservacao) {
		this.colNCObservacao = colNCObservacao;
	}

	public TableColumn<Segmento, String> getColFornecedor() {
		return colFornecedor;
	}

	public void setColFornecedor(TableColumn<Segmento, String> colFornecedor) {
		this.colFornecedor = colFornecedor;
	}

	public TableColumn<Segmento, String> getColSeuNro() {
		return colSeuNro;
	}

	public TableColumn<Segmento, String> getColVencimento() {
		return colVencimento;
	}

	public void setColSeuNro(TableColumn<Segmento, String> colSeuNro) {
		this.colSeuNro = colSeuNro;
	}

	public void setColVencimento(TableColumn<Segmento, String> colVencimento) {
		this.colVencimento = colVencimento;
	}

	public TableColumn<Segmento, BigDecimal> getColValorTituloNominal() {
		return colValorTituloNominal;
	}

	public void setColValorTituloNominal(
			TableColumn<Segmento, BigDecimal> colValorTituloNominal) {
		this.colValorTituloNominal = colValorTituloNominal;
	}

	public TableColumn<Segmento, String> getColNCSeuNro() {
		return colNCSeuNro;
	}

	public void setColNCSeuNro(TableColumn<Segmento, String> colNCSeuNro) {
		this.colNCSeuNro = colNCSeuNro;
	}
	
	
}
