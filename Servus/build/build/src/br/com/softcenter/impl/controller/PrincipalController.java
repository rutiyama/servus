package br.com.softcenter.impl.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;

import br.com.softcenter.config.connection.ConnectionConfig;
import br.com.softcenter.impl.model.bean.GridFaturasJavaFXBean;
import br.com.softcenter.impl.model.bean.ParametrosSistema;
import br.com.softcenter.impl.model.bean.Segmento;
import br.com.softcenter.impl.model.bean.SegmentoT;
import br.com.softcenter.utils.alerta.MensagemTipo;
import br.com.softcenter.utils.alerta.ShowMessage;
import br.com.softcenter.utils.database.DatabaseUtils;
import br.com.softcenter.utils.window.DiretorioUtils;
import br.com.softcenter.utils.window.ThreadUtils;
import br.com.softcenter.utils.window.WindowUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since  08-09-2015
 */
public class PrincipalController implements Initializable{

	@FXML
	private Button btnContaReduzida,btnSelecionarImagem, 
		btnCarregar, btnSelecionarGridOK, btnSelecionarGridCancelar, btnConfirmarRetorno, btnSair, btnConfiguracao;
	@FXML
	private TextField edtNomeDoArquivo, edtContaReduzidaDescricao, edtContaReduzida;
	private Stage stage;
	@FXML
	private TableView<Segmento> gridFaturas, gridFaturasNaoConformidade;
	@FXML
	private TableColumn<Segmento, String> colSegmento, colTituloEmpresa, colTituloBanco, colNCTitEmpresa,colNCTitBanco;
	@FXML
	private TableColumn<Segmento, String> colDataEfetivacaoCredito, colNCDataCredito, colNCFornecedor, colNCSeuNro, colNCObservacao, colFornecedor, colSeuNro, colVencimento;
	@FXML
	private TableColumn<Segmento, BigDecimal> colValorDiferenca, colValorPagoSacado, colNCVlrDiferenca, colNCVlrSacado, colValorTituloNominal;
	@FXML
	private TableColumn<Segmento, ImageView> colSituacao;
	@FXML
	private TableView<Segmento> table;
	@FXML
	private Label tituloGridFatura, tituloGridFaturaNC, edtStatus;
	@FXML
	private Pane  panebottom,paneFaturasLiquidadas;
	@FXML
	private TitledPane titledpane;
	@FXML
	private Separator separadorFatura;
	@FXML
	private Label lblQuantidadeResultado, lblVlrTotalTitulosResultado, lblVlrTotalCobradoResultado;
	
	private List<Segmento> segmentos;
	private List<Segmento> segmentosNaoConformidade;
	private List<String> duplicados;
	
	private static final boolean DEBUG = false;
	private String mensagemResultadoOperacao = null;
	
	private Logger logger;
	
	//Método executado ao iniciar esta classe (Ao "mostrar" a janela):
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		edtNomeDoArquivo.textProperty().addListener((observable, oldvalue, newvalue) ->{
			if(edtNomeDoArquivo.getText().length() > 0)
				btnCarregar.setDisable(false);
			else
				btnCarregar.setDisable(true);
		}); 
	}
	
	//Construtor da classe que recebe o stage:
	public PrincipalController(Stage stage) {
		super();
		this.stage = stage;
		
		logger = Logger.getLogger("br.com.softcenter.impl.controller");
	}
	
	//Método responsável pelo evento de Clique, do botão (imagem) conta reduzida:
	@FXML
	protected void btnContaReduzidaClick(ActionEvent e){
		//Instancia o formulário para selecionar a Conta Reduzida e mostra ao usuário:
		WindowUtils.showFXML(getClass(), stage, "../view/SelecionarContaReduzida.fxml", "Selecionar Conta Reduzida", 
				new SelecionarContaReduzidaController(stage, this));
	}
	
	//Método responsável pelo evento de Clique do botão selecionar arquivo de Retorno:
	@FXML
	protected void btnSelecionarImagemClick(ActionEvent e){
		String s = DiretorioUtils.selectFile("Arquivos de Retorno", stage, "*.ret",  "*.txt");
		if (s != null && !s.equals("")){
			edtNomeDoArquivo.setText(s);
			if(edtNomeDoArquivo.getText().length() > 0)
				btnCarregar.setDisable(false);
			else
				btnCarregar.setDisable(true);
		}
	}
	
	@FXML 
	protected void edtNomeDoArquivoKeyPress(KeyEvent keyEvent){
		if(keyEvent.getCode() == KeyCode.F4){
			btnSelecionarImagemClick(null);
		}
	}
	
	@FXML
	protected void btnSairClick(ActionEvent e){
		Platform.exit();
	}
		
	@FXML
	protected void btnCarregarClick(ActionEvent e){
		SplashScreen splash = new SplashScreen();
		
		/* Limpa a lista se caso já foi feita a instância, 
		 * para evitar registros duplicados na segunda importação */
		btnConfirmarRetorno.setDisable(true);
		if(segmentosNaoConformidade != null)
			segmentosNaoConformidade.clear();
		if(segmentos != null)
			segmentos.clear();
		
		//reseta para que não exapanda nenhum accordeon
		
		edtStatus.setText("Carregando arquivo... Por favor, aguarde...");
		btnCarregar.setDisable(true);
		
		/* Thread responsável por carregar o arquivo e popular o ArrayList. Foi criado separadamente esse processo
		 * para evitar o travamento sem feedback ao usuário: */
		try{
			splash.init();
			splash.start(new Stage(), stage);
		}catch(Exception ex){
			logger.debug("Erro ao iniciar Splash Screen",ex);
		}
		
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try{
					GridFaturasJavaFXBean beanGridFaturas = 
							new GridFaturasJavaFXBean(gridFaturas, colSegmento, colTituloEmpresa, colDataEfetivacaoCredito, 
												  colTituloBanco, colValorDiferenca, colValorPagoSacado, table,colSituacao, colFornecedor, 
												  colSeuNro, colVencimento, colValorTituloNominal);
					
					GridArquivosDeRetornoController gridController = 
							new GridArquivosDeRetornoController(beanGridFaturas, edtNomeDoArquivo.getText());
					
					ArquivoRetornoController leitorRetorno = new ArquivoRetornoController(edtNomeDoArquivo.getText());
					segmentos = leitorRetorno.lerArquivoRetorno();
					//Método para iniciar a verificação dos títulos em não conformidade:
					configurarGridNaoConformidade();
					gridFaturas.getItems().setAll(segmentos);
					gridController.setDuplicados(duplicados);
					gridController.popularGrid();
					btnConfirmarRetorno.setDisable(desativarBotaoConfirmarRetorno());	
					
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							tituloGridFatura.setText(segmentos.size() + " Fatura(s)/Duplicata(s) liquidada(s) pelo Banco");
							ThreadUtils.ExibirMensagemErroThread(edtStatus, "Arquivo carregado com sucesso!");
							lblQuantidadeResultado.setText(String.valueOf(segmentos.size()));
							lblVlrTotalTitulosResultado.setText(calcularTotalTitulos().setScale(2).toString());
							lblVlrTotalCobradoResultado.setText(calcularTotalCobrado().setScale(2).toString());
						}
					});
					
					splash.end();
				}catch(Exception ex){
					String mensagem = "Falha na leitura do arquivo." + ex;
					btnCarregar.setDisable(true);
					ex.printStackTrace();
					splash.end();
					 if (DEBUG) {
					    String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();
					    String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
					    String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
					    int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
					    mensagem = "Classe: " + className + "\nMétodo: " + methodName + "\nLinha" + lineNumber;
					 }
					ShowMessage.Exibir(mensagem, MensagemTipo.ERRO);
					ThreadUtils.ExibirMensagemErroThread(edtStatus, mensagem);
					logger.debug("Erro na Leitura",ex);
				}
			} 
		});
		t.start();
		
	}	
	
	
	//Método para verificar se o botão "Confirmar Retorno" será ativado ou não:
	private boolean desativarBotaoConfirmarRetorno(){
		/* Se o ArrayList de "Segmentos" e o ArrayList de "Não conformidade" forem iguais, ou seja, 
		 * todos os títulos estão em não conformidade: */ 
		boolean desativar = ((segmentos.size() <= 0) || (segmentosNaoConformidade.size() == segmentos.size()) );
		
		/*
		 * Se desativar estiver falso, mas existe registro duplicados, desativar fica true:
		 */
		if(!desativar){
			if(duplicados != null && duplicados.size() > 0){
				desativar = true;
			}
		}
		return desativar;
	}
	
	/* Método responsável por preparar a grid de "Não Conformidade": */
	private void configurarGridNaoConformidade(){
		//Grid arquivos não conformidade:
		segmentosNaoConformidade = new ArrayList<Segmento>();
		
		GridFaturasJavaFXBean gridNaoConformidadeBean = 
				new GridFaturasJavaFXBean(gridFaturasNaoConformidade, colSegmento, colNCTitEmpresa, 
						colNCDataCredito, colNCTitBanco, colNCVlrDiferenca, colNCVlrSacado, 
						gridFaturasNaoConformidade, colNCObservacao, colSituacao, colNCFornecedor, colNCSeuNro );
		
		GridArquivosDeRetornoController gridController = 
				new GridArquivosDeRetornoController(gridNaoConformidadeBean, edtNomeDoArquivo.getText());

		verificarConformidadeArquivoRetorno();
 
		
		gridFaturasNaoConformidade.getItems().setAll(segmentosNaoConformidade);
		gridController.setDuplicados(duplicados);
		gridController.popularGrid();
		
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				tituloGridFaturaNC.setText(segmentosNaoConformidade.size()+ " Fatura(s)/Duplicata(s) com não conformidade(s)");
			}
		});
	}

	//Método responsável apenas por realizar a validação do arquivo de retorno. Este método é apenas para
	//popular a grid de títulos em não conformidade:
	private void verificarConformidadeArquivoRetorno(){
		duplicados = nossoNumeroDuplicados();
		
		for(Segmento segmento : segmentos){
			String mensagem = null;
			boolean isConformidade = true;
			SegmentoT seg = (SegmentoT)segmento;
			
			if(!seg.getCodigoMovimento().equals("06") && !seg.getCodigoMovimento().equals("17")){
				isConformidade = false;
			}else if(segmentoJaImportado(seg)){
				isConformidade = false;
				mensagem = "O título com o nosso número " + seg.getIdentificacaoNossoNumero() + " já foi importado.";
			}else if(duplicados.indexOf(seg.getIdentificacaoNossoNumero()) != -1){
				isConformidade = false;
				seg.setDuplicado(true);
				mensagem = "O título com o Nosso Número " + seg.getIdentificacaoNossoNumero() + " está duplicado!";
			}else if(seg.getSegmentoU().getDataEfetivacaoCredito().before(DatabaseUtils.RetornarDataLimiteLancamentoFilial()) ){
				isConformidade = false;
				mensagem = "A data de baixa é anterior a Data de Lançamento na filial";
			}else if(seg.getIdentificacaoNossoNumero() != null && !seg.getIdentificacaoNossoNumero().equals("")){

				String situacao = DatabaseUtils.RetornarSituacaoDoTitulo(Long.parseLong(seg.getIdentificacaoNossoNumero()));
				Long titulo     = DatabaseUtils.RetornarIdTituloPorNossoNumero(Long.parseLong(seg.getIdentificacaoNossoNumero()));

				if(titulo == -1L){
					mensagem = "O Título não  está cadastrado no sistema.";
				}else if (situacao != null && situacao.trim().toLowerCase().equals("c")){
					mensagem = "O Título está cancelado.";
				}
				
				if(mensagem != null){
					isConformidade = false; 
				}
			}
			
			if(!isConformidade){
				seg.setObservacao(mensagem);
				seg.setEstaEmConformidade(false);
				segmentosNaoConformidade.add(seg);
			}
		}
	}
	
	private List<String> nossoNumeroDuplicados(){
		List<String> duplicados = new ArrayList<String>();
		HashSet<String> set = new HashSet<String>();
		
		for(Segmento segmento : segmentos){
			if(set.add(((SegmentoT)segmento).getIdentificacaoNossoNumero()) == false)
				duplicados.add(((SegmentoT)segmento).getIdentificacaoNossoNumero());
		}
		
		return duplicados;
	}
	
	private boolean segmentoJaImportado(SegmentoT segmento){
		return DatabaseUtils.VerificarSeTituloJaFoiImportado(segmento.getSegmentoU().getDataEfetivacaoCredito(), 
				segmento.getIdentificacaoNossoNumero());
	}
	
	/* Método responsável pelo botão de "Confirmar o Retorno": */
	@FXML
	protected void btnConfirmarRetornoClick(ActionEvent e) throws Exception{
		SplashScreen splash = new SplashScreen();
		
		try{
			splash.init();
			splash.start(new Stage(), stage);
			btnSair.setDisable(true);
		}catch(Exception ex){
			logger.debug("Erro ao iniciar Splash Screen ao confirmar retorno",ex);
		}
		
		Thread tConfirmarRetorno = new Thread(new Runnable() {
			
			@Override
			public void run() {
				PopularRetornoController retornoController = null;
				mensagemResultadoOperacao = "Ocorreu um problema ao realizar a importação, verifique a conexão e o arquivo a ser importado...";
				
				try{
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							edtStatus.setText("Importando Títulos... Aguarde...");							
						}
					});
					retornoController = new PopularRetornoController(segmentos);
					retornoController.setSegmentosNaoConformidade(segmentosNaoConformidade);
					retornoController.confirmarRetorno();
				}catch(Exception ex){
					try{
						ConnectionConfig.getConn().rollback();
						btnSair.setDisable(false);
						splash.end();
					}catch(Exception e){
						logger.debug("Erro ao dar rollback",e);
					}
					
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							ShowMessage.Exibir(ex.toString(), MensagemTipo.ERRO);
							edtStatus.setText("Ocorreu um problema ao realizar a importação, verifique a conexão e o arquivo a ser importado...");
						}
					});
					logger.debug("Erro ao Confirmar retorno",ex);
					return;
				}
				
				try{
					/* Primeira verificação: Verifica se a quantidade de retornos inválidos é maior que zero e se não é todos os registros: */
					if(retornoController.getQuantidadeRetornosInvalidosNaGravacao() > 0 && retornoController.getQuantidadeRetornosInvalidosNaGravacao() != segmentos.size()){
						mensagemResultadoOperacao = retornoController.getQuantidadeRetornosInvalidosNaGravacao() + " das " + segmentos.size() + " faturas não foram importadas.";
						ConnectionConfig.getConn().commit();
					}else if (retornoController.getQuantidadeRetornosInvalidosNaGravacao() == 0){
						mensagemResultadoOperacao = "Todos os títulos foram importados com sucesso!";
						ConnectionConfig.getConn().commit();
					}
					
					splash.end();
					btnSair.setDisable(false);
					btnConfirmarRetorno.setDisable(true);
				}catch(Exception ex){
					try{
						ConnectionConfig.getConn().rollback();
						btnSair.setDisable(false);
						splash.end();
					}catch(Exception e){
						logger.debug("Erro ao dar rollback",e);
					}
					
					logger.debug("Erro ao commitar",ex);
					splash.end();
					btnSair.setDisable(false);
				}

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						edtStatus.setText(mensagemResultadoOperacao);
						ShowMessage.Exibir(mensagemResultadoOperacao, MensagemTipo.INFORMACAO);
					}
				});
			}
		});
		
		tConfirmarRetorno.start();
	}
	
	protected TextField retornarTextFieldContaReduzida(){
		return edtContaReduzida;
	}
	
	public TextField retornarTextFieldConta(){
		return edtContaReduzidaDescricao;
	}
	
	protected Button retornarBotaoConfirmar(){
		return btnSelecionarGridOK;
	}
	
	protected Button retornarBotaoCancelar(){
		return btnSelecionarGridCancelar;
	}
	
	private BigDecimal calcularTotalTitulos(){
		BigDecimal total = new BigDecimal(0);
		for (Segmento seg : segmentos){
			//Valor total do titulo, sem descontos:
			BigDecimal totalTmp = ((SegmentoT)seg).getValorDoTituloNominal().get();
			total = total.add(totalTmp);
		}
		return total;
	}
	
	private BigDecimal calcularTotalCobrado(){
		BigDecimal total = new BigDecimal(0);
		for (Segmento seg : segmentos){
			
			//Valor total do titulo, sem descontos:
			BigDecimal totalTmp = ((SegmentoT)seg).getSegmentoU().getValorPagoSacado();
			total = total.add(totalTmp);
		}
		return total;
	}
	
	@FXML
	protected void btnConfiguracaoOnClick(ActionEvent e) throws IOException{
//		WindowUtils.showFXML(getClass(), stage, "../view/ParametrosSistema.fxml", "Configuração do Sistema", 
//				new ParametroSistemaController());
		
      
       Pane fxmlLoader = FXMLLoader.load(getClass().getResource("/br/com/softcenter/impl/view/ParametrosSistema.fxml") ); 
       Stage stage = new Stage();
       stage.initModality(Modality.APPLICATION_MODAL);
       stage.initStyle(StageStyle.UTILITY);
       stage.setTitle("Parâmetros do Sistema Servus SOFTCENTER");
       stage.setScene(new Scene(fxmlLoader));  
       stage.show();
       
	}
}
