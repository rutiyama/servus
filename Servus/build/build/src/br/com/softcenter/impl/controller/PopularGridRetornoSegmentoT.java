package br.com.softcenter.impl.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableRow;
import javafx.scene.image.ImageView;
import br.com.softcenter.impl.model.PopularGridSegmentoStrategy;
import br.com.softcenter.impl.model.bean.GridFaturasJavaFXBean;
import br.com.softcenter.impl.model.bean.Segmento;
import br.com.softcenter.impl.model.bean.SegmentoT;

/**
 * @author Rodrigo Junior Utiyama
 * @since  09-09-2015
 */

public class PopularGridRetornoSegmentoT implements PopularGridSegmentoStrategy{
	private boolean isDuplicado;
	
	@Override
	public void popularGridRetornoJavaFx(GridFaturasJavaFXBean gridFaturasBean,List<String>  duplicados) {

		if(duplicados != null && !duplicados.isEmpty()){
			gridFaturasBean.getColTituloBanco().setCellFactory(column -> {
				return new TableCell<Segmento, String>() {
					@Override
					protected void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						setText(empty ? "" : getItem().toString());
						setGraphic(null);

						TableRow<Segmento> currentRow = getTableRow();

						if (!isEmpty()) {
							if (duplicados.indexOf(item.toString()) != -1)
								currentRow
										.setStyle("-fx-background-color:lightcoral");
							else
								currentRow.setStyle("");
							
						}
					}
				};
			});
		}
		
		//Coluna Titulo Empresa 
		gridFaturasBean.getColTituloEmpresa().
			setCellValueFactory(cellData -> {
				isDuplicado = ((SegmentoT)cellData.getValue()).isDuplicado();
				String s = ((SegmentoT)cellData.getValue()).getIdentificadorBanco();
				if(s != null)
						return new SimpleStringProperty(s.replaceFirst("^0+(?!$)", ""));
				
				return new SimpleStringProperty("");					
			});
		
		//Coluna Título Banco:
		gridFaturasBean.getColTituloBanco().
			setCellValueFactory(cellData -> {
				String s = ((SegmentoT)cellData.getValue()).getIdentificacaoNossoNumeroProperty().get();
				return new SimpleStringProperty(s.replaceFirst("^0+(?!$)", ""));
			});
		
		if(gridFaturasBean.getColNCObservacao() != null){
			gridFaturasBean.getColNCObservacao().
			setCellValueFactory(cellData -> {
				String s = ((SegmentoT)cellData.getValue()).getObservacao();
				return new SimpleStringProperty(s);
			});
		} 
		
		
		gridFaturasBean.getColSituacao().setCellValueFactory(cellValue-> {
			ImageView imgView = new ImageView("/br/com/softcenter/config/imagens/ok4.png");
			//Se o registro não foi importado e também não há nenhuma inconformidade:
			if(cellValue.getValue().isEstaEmConformidade())
				return new SimpleObjectProperty<ImageView>(imgView);
			
			return new SimpleObjectProperty<ImageView>(new ImageView("/br/com/softcenter/config/imagens/triangle38.png"));
		}); 
		
		if(gridFaturasBean.getColSeuNro() != null){
			gridFaturasBean.getColSeuNro().setCellValueFactory(cellData-> {
				String s = ((SegmentoT)cellData.getValue()).getIdentificacaoSeuNumero().get();
				return  new SimpleStringProperty(s);
			}); 
		}
		
		if(gridFaturasBean.getColVencimento() != null){
			gridFaturasBean.getColVencimento().setCellValueFactory(cellData-> {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String data = sdf.format(((SegmentoT)cellData.getValue()).getDataVencimento().get());
				return new SimpleStringProperty(data);
			}); 
		}
		
//		if(gridFaturasBean.getColValorTituloNominal() != null){
//			gridFaturasBean.getColValorTituloNominal().setCellValueFactory(cellData-> {
//				return ((SegmentoT)cellData.getValue()).getValorDoTituloNominal();
//			}); 
//		}
		
		if(gridFaturasBean.getColValorTituloNominal() != null){
			gridFaturasBean.getColValorTituloNominal().
			setCellValueFactory(cellData -> {
				BigDecimal bd = ((SegmentoT)cellData.getValue()).getValorDoTituloNominal().get().setScale(2);
				((SegmentoT)cellData.getValue()).getValorDoTituloNominal().set(bd);
				return ((SegmentoT)cellData.getValue()).getValorDoTituloNominal();
			});
		}
		
		
		if(gridFaturasBean.getColFornecedor() != null){
			gridFaturasBean.getColFornecedor().
			setCellValueFactory(cellData -> {
				String s = ((SegmentoT)cellData.getValue()).getNomeSacado();
				return new SimpleStringProperty(s);
			});
		}
		
	}
}
