package br.com.softcenter.impl.controller;

import java.io.Serializable;
import java.util.List;

import br.com.softcenter.impl.model.bean.GridFaturasJavaFXBean;

/**
 * @author Rodrigo Junior Utiyama
 * @since  09-09-2015
 */

public class GridArquivosDeRetornoController implements Serializable{
	
	private GridFaturasJavaFXBean beanGrid;
	private List<String> duplicados;
	//private String urlArquivoRetorno;
	
	public GridArquivosDeRetornoController(GridFaturasJavaFXBean gridFaturasBean, String urlArquivoRetorno){
		this.beanGrid = gridFaturasBean;
		//this.urlArquivoRetorno = urlArquivoRetorno;
	}
	public void setDuplicados(List<String> duplicados){
		this.duplicados = duplicados;
	}

	public void popularGrid(){
		PopularGridArquivoRetornoFactory.popularGrid(beanGrid,  duplicados );
	}
}