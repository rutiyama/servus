package br.com.softcenter.impl.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import br.com.softcenter.impl.model.IDAOContaBancoImpl;
import br.com.softcenter.impl.model.bean.ContaBanco;

/**
 * @author Rodrigo Junior Utiyama
 * @since  08-09-2015
 */

public class SelecionarContaReduzidaController implements Initializable{

	@FXML
	private TableView<ContaBanco> gridSelecionarContaReduzida;
	private List<ContaBanco> contas;
	
	@FXML
	private TableColumn<ContaBanco, String> colBanco, 
						colAgencia, colConta, colTipoConta;
	@FXML
	private TableColumn<ContaBanco, Long> colNumBanco, colCodContabil;
	
	@FXML
	private Button btnSelecionarGridOK, btnCancelarClick;
	
	private Stage stage;
	private ContaBanco contaBancoSelecionado;
	private PrincipalController controllerParent;
	
	
	public SelecionarContaReduzidaController(Stage stage, PrincipalController controllerParent) {
		this.stage = stage;
		this.controllerParent = controllerParent;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		gridSelecionarContaReduzida.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		popularGrid();
		gridSelecionarContaReduzida.getItems().setAll(contas);
		configurarGridClick();
	}
	
	@FXML
	protected void btnOKClick(ActionEvent event){
		popularCamposContaFormPrincipal(event);
	}
	
	@FXML 
	protected void btnCancelarClick(ActionEvent event){
		((Node)(event.getSource())).getScene().getWindow().hide();
	}
	
	private void popularGrid(){
		contas = new IDAOContaBancoImpl().select();
		colBanco.setCellValueFactory(cellData -> cellData.getValue().getBanco().get().descricaoProperty());
		colNumBanco.setCellValueFactory(cellData -> cellData.getValue().getBanco().get().numeroProperty().asObject());
		colAgencia.setCellValueFactory(cellData -> cellData.getValue().agenciaProperty());
		colConta.setCellValueFactory(cellData -> cellData.getValue().contaNumeroProperty());
		colTipoConta.setCellValueFactory(cellData -> cellData.getValue().getConta().get().tipoProperty());
		colCodContabil.setCellValueFactory(cellData -> cellData.getValue().getConta().get().codigoContabil().asObject());
	}
	
	private void configurarGridClick(){
		gridSelecionarContaReduzida.setRowFactory(rf -> {
			TableRow<ContaBanco> row = new TableRow<ContaBanco>();
			row.setOnMouseClicked(event -> {
				contaBancoSelecionado = row.getItem();
				
				if (event.getClickCount() == 2 && (!row.isEmpty())){
					popularCamposContaFormPrincipal(event);
				}else{
					btnSelecionarGridOK.setDisable(false);
				}
			});
			return row;
		});
	}
	
	private void popularCamposContaFormPrincipal(Event event){
		if (contaBancoSelecionado != null){
			String codigoContabil = String.valueOf(contaBancoSelecionado.getConta().get().getNumeroBanco());
			this.controllerParent.retornarTextFieldContaReduzida().setText(codigoContabil);
			this.controllerParent.retornarTextFieldConta().setText(contaBancoSelecionado.getConta().get().getDescricao());
			((Node)(event.getSource())).getScene().getWindow().hide();
		}else{
			btnSelecionarGridOK.setDisable(true);
		}
	}
}
