package br.com.softcenter.impl.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.softcenter.config.connection.ConnectionConfig;
import br.com.softcenter.impl.model.bean.Banco;
import br.com.softcenter.impl.model.bean.Conta;
import br.com.softcenter.impl.model.bean.ContaBanco;
import br.com.softcenter.impl.model.bean.SegmentoT;
 
/**
 * @author Rodrigo Junior Utiyama
 * @since  03-09-2015
 */

public class IDAOContaBancoImpl implements IDAOModel<ContaBanco, Integer>{
	
	private Connection conn;
	private PreparedStatement pstmt;
	
	public IDAOContaBancoImpl(){
		conn = ConnectionConfig.getConn();
	}	
	
	@Override
	public List<ContaBanco> select() {
		String sql = "SELECT " 											   +
					 "	banco.BNC_DESCRICAO, banco.BNC_NUMERO, banco.BNC_ID,		 				" +
					 "	conta.CON_TIPO, conta.CON_CODIGOCONTABIL, conta.CON_DESCRICAO, conta.CON_ID, " +
				 	 "	contabanco.CNB_AGENCIA, contabanco.CNB_CONTA, 	 				" +
					 "  contabanco.CNB_DVAGENCIA, contabanco.CNB_DVCONTA 				" +
			 	 	 "FROM contabanco " +
			 	 	 "LEFT JOIN banco ON (contabanco.BNC_ID = banco.BNC_ID)  " +
			 	 	 "LEFT JOIN conta ON (contabanco.CON_ID = conta.con_id)";
		
		List<ContaBanco> lista = new ArrayList<ContaBanco>();
		ResultSet result = null;
		
		try{
			pstmt  = conn.prepareStatement(sql);
			result = pstmt.executeQuery();
			
			while(result.next()){
				lista.add(retornarSelect(result));
			}
			pstmt.close();
			
			return lista;
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
		return null;
	}

	@Override
	public ContaBanco select(Integer key) {
		return null;
	}
	
	private ContaBanco retornarSelect(ResultSet rs) throws SQLException{
		Conta conta = new Conta();
		Banco banco = new Banco();
		ContaBanco contaBanco = new ContaBanco();
		
		conta.setNumeroBanco(rs.getString("BNC_NUMERO"));
		conta.setCodigoContabil(rs.getLong("CON_CODIGOCONTABIL"));
		conta.setDescricao(rs.getString("CON_DESCRICAO"));
		conta.setId(rs.getLong("CON_ID"));
		conta.setTipo(rs.getString("CON_TIPO"));
		
		banco.setId(rs.getLong("BNC_ID"));
		banco.setDescricao(rs.getString("BNC_DESCRICAO"));
		banco.setNumero(rs.getLong("BNC_NUMERO"));
		
		contaBanco.setBanco(banco);
		contaBanco.setConta(conta);
		contaBanco.setAgencia(rs.getString("CNB_AGENCIA"));
		contaBanco.setContaNumero(rs.getString("CNB_CONTA"));
		
		return contaBanco;
	}

	@Override
	public ContaBanco select(Integer key, String where) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(SegmentoT segmento) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(SegmentoT segmento) {
		// TODO Auto-generated method stub
	}
}
