package br.com.softcenter.utils.alerta;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */
public class MensagemInformacao implements InterfaceCaixaMensagem{

	@Override
	public void Exibir(String mensagem) {
		Alert alerta = new Alert(AlertType.INFORMATION);
		alerta.setTitle("Informação");
		alerta.setHeaderText("");
		alerta.setContentText(mensagem);
		alerta.showAndWait();
	}
}
