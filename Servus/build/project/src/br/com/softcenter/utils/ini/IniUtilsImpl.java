package br.com.softcenter.utils.ini;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import br.com.softcenter.impl.model.bean.BeanIniUtils;
import br.com.softcenter.utils.alerta.MensagemTipo;
import br.com.softcenter.utils.alerta.ShowMessage;

/**
 * 
 * @author Rodrigo Junior Utiyama
 * @since 03-09-2015
 *
 */
public class IniUtilsImpl implements IniUtils {

	private Properties prop;
	private String propertyFile;
	private InputStream inputStream;
	private BeanIniUtils beanFirbird;
	
	public IniUtilsImpl(){
		this.prop = new Properties();
		//this.propertyFile = "../../mysqlconfig.properties";
		this.propertyFile = "mysqlconfig.properties";
//		this.inputStream = getClass().getResourceAsStream(this.propertyFile);
		this.inputStream = ClassLoader.getSystemResourceAsStream(this.propertyFile);
	}	
	
	/*
	 *  
	 * @see br.com.softcenter.cra.utils.arquivoini.IniUtils#selectIni()
	 * M�todo que implementa a interface IniUtils, sendo respons�vel por retornar
	 * uma inst�ncia da classe BeanIniUtils, contendo informa��es do arquivo Property
	 * do Firebird:
	 */
	@Override
	public BeanIniUtils selectIni() {

		if (readProperty() != null && this.prop != null){

			beanFirbird = new BeanIniUtils(
					prop.getProperty("PROP.DATABASE_URL"), 
					prop.getProperty("PROP.DATABASE_USER"),
					prop.getProperty("PROP.DATABASE_PASS"),
					prop.getProperty("PROP.DATABASE_ENCODING"));

			return beanFirbird;
		}	
		
		return null;
	}
	
	/*
	 * M�todo respons�vel por "carregar" o arquivo property:
	 */
	private Properties readProperty(){
		try{
			if (inputStream != null)
				prop.load(inputStream);
		}catch(IOException ioex){
			ioex.printStackTrace();
		}
		return this.prop;
	}

}
