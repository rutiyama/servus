package br.com.softcenter.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author Rodrigo Junior Utiyama
 * @since  09-09-2015
 */

public class DateUtils{
	public static java.sql.Date ConvertUtilToSQLDate(java.util.Date data){
	     java.sql.Date sqlDate = new java.sql.Date(data.getTime());
	     return sqlDate;
	}
	
	public static String ConvertToFirebirdDateString(java.util.Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy H:mm");
		return  formatter.format(date);
	}
	
	public static String ConvertToMySQLDateString(java.util.Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return  formatter.format(date);
	}
	
	public static java.util.Date ConvertStringToUtilDate(String data) throws ParseException{
		SimpleDateFormat formatter = new  SimpleDateFormat("yyyy-MM-dd");
		return formatter.parse(data) ;
	}
	
	public static String ConvertDateUtilToString(java.util.Date data){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return  formatter.format(data);
	}
	
	public static String FormatDateStringToMySQL(String data){
		data =   (data.substring(4,8) + "-" + data.substring(2, 4) + "-" +  data.substring(0,2));
		return data;
	}
}
