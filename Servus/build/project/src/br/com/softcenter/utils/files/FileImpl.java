package br.com.softcenter.utils.files;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
 

public class FileImpl{

	private Path path;
	private boolean createFileIfNotExists;
	
	public void create(boolean createFileIfNotExists, String filename,
			String path) {
		/*
		 * Se o path estiver vazio, indica que o arquivo será criado no diretório da aplicação,
		 * portanto, se for informado o path e não terminar com um "/", é inserido automaticamente para evitar erros:
		 */
		if(!path.isEmpty() && !path.endsWith("/"))
			path.concat("/");
		
		this.path = Paths.get(path);
		this.createFileIfNotExists = createFileIfNotExists;
	}
	
	public FileImpl writeAppend(String text) throws IOException{
		validateWrite();
		Files.write(path, text.getBytes(), StandardOpenOption.APPEND);
		return this;
	}
	
	public FileImpl writeNew(String text) throws IOException{
		validateWrite();
		Files.write(path, text.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
		return this;
	}
 
	private void validateWrite()  throws IOException{
		if(!Files.exists(path, LinkOption.NOFOLLOW_LINKS) && createFileIfNotExists)
			Files.createFile(path);
		else
			throw new FileNotFoundException("O caminho especificado não existe.");
	}
}
