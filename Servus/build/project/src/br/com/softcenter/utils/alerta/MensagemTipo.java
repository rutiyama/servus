package br.com.softcenter.utils.alerta;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */
public enum MensagemTipo {
	ALERTA(1), INFORMACAO(2), ERRO(3);
	
	private int tipoMensagem;
	
	private MensagemTipo(int tipo) {
		this.tipoMensagem = tipo;
	}
	
	public int getTipoMensagem(){
		return this.tipoMensagem;
	}
}
