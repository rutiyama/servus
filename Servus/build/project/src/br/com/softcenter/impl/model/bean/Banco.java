package br.com.softcenter.impl.model.bean;

import java.io.Serializable;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Rodrigo Junior Utiyama
 * @since  02-09-2015
 */


public class Banco implements Serializable{
	private SimpleLongProperty 		id;
	private SimpleStringProperty  	descricao;
	private SimpleLongProperty		numero;
	
	public SimpleLongProperty idProperty(){
		return id;
	}
	
	public SimpleStringProperty descricaoProperty(){
		return descricao;
	}
	
	public SimpleLongProperty numeroProperty(){
		return numero;
	}
	public long getId() {
		return id.get();
	}
	public String getDescricao() {
		return descricao.get();
	}
	public long getNumero() {
		return numero.get();
	}
	public void setId(long id) {
		this.id = new SimpleLongProperty(id);
	}
	public void setDescricao(String descricao) {
		this.descricao = new SimpleStringProperty(descricao);
	}
	public void setNumero(long numero) {
		this.numero =  new SimpleLongProperty(numero);
	}

	@Override
	public String toString() {
		return "Banco [id=" + id + ", descricao=" + descricao + ", numero="
				+ numero + "]";
	}
}
