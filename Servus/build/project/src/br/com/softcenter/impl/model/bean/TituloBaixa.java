package br.com.softcenter.impl.model.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since  10-09-2015
 */

public class TituloBaixa {
	private long id;
	private Date data;
	private BigDecimal valor;
	private BigDecimal juro;
	private BigDecimal desconto;
	private Character tipo;
	
	public TituloBaixa(){}
	
	public TituloBaixa(long id, Date data, BigDecimal valor, BigDecimal juro,
			BigDecimal desconto, Character tipo) {
		super();
		this.id = id;
		this.data = data;
		this.valor = valor;
		this.juro = juro;
		this.desconto = desconto;
		this.tipo = tipo;
	}
	
	public long getId() {
		return id;
	}
	public Date getData() {
		return data;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public BigDecimal getJuro() {
		return juro;
	}
	public BigDecimal getDesconto() {
		return desconto;
	}
	public Character getTipo() {
		return tipo;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public void setJuro(BigDecimal juro) {
		this.juro = juro;
	}
	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}
	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "TituloBaixa [id=" + id + ", data=" + data + ", valor=" + valor
				+ ", juro=" + juro + ", desconto=" + desconto + ", tipo="
				+ tipo + "]";
	}
}
