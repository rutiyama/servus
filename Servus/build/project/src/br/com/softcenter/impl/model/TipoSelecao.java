package br.com.softcenter.impl.model;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */
public enum TipoSelecao {
	DATA_POSTERIOR(1), DATA_ANTERIOR(2);
	
	private int tipo;
	
	private TipoSelecao(int tipo){
		this.tipo = tipo;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	
	
}
