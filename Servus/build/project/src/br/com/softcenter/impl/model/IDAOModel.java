package br.com.softcenter.impl.model;

import java.sql.SQLException;
import java.util.List;

import br.com.softcenter.impl.model.bean.SegmentoT;

/**
 * @author Rodrigo Junior Utiyama
 * @since  03-09-2015
 */

public interface IDAOModel <T, t>{
	public List<T> select();
	public T select(t key) throws SQLException;
	public T select(t key, String where);
	public void insert(SegmentoT segmento);
	public void update(SegmentoT segmento);
}
