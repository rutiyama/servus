package br.com.softcenter.impl.model;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softcenter.config.connection.ConnectionConfig;
import br.com.softcenter.impl.model.bean.Boleto;
import br.com.softcenter.impl.model.bean.SegmentoT;
import br.com.softcenter.utils.date.DateUtils;


/**
 * @author Rodrigo Junior Utiyama
 * @since  10-09-2015
 */
public class IDAOBoletoImpl implements IDAOModel<Boleto, Long>{

	private Connection conn;
	
	public IDAOBoletoImpl() {
		conn = ConnectionConfig.getConn();
	}
	
	@Override
	public List<Boleto> select() {
		String sql = "SELECT * FROM apr.boleto";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Boleto> boletos = new ArrayList<Boleto>();
		
		try{
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				boletos.add(select(rs.getLong("BLT_ID")));
			}
			
			return boletos;
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		return null;
	}

	@Override
	public Boleto select(Long key) {
		String sql = "SELECT * FROM apr.boleto WHERE BLT_ID = ?";
		PreparedStatement pstmt = null;
		ResultSet 		 	 rs = null;
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1,key);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				Boleto boleto = new Boleto(rs.getLong("BLT_ID"), 
						DateUtils.ConvertStringToUtilDate(rs.getString("BLT_DATAEMISSAO")), 
						new BigDecimal(rs.getString("BLT_VALORTITULO")), 
						new BigDecimal(rs.getString("BLT_VALORTAXA")),
						new BigDecimal(rs.getString("BLT_NOSSONUMERO")));
				
				pstmt.close();
				rs.close();
				
				return boleto;
			}
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}catch(ParseException pex){
			pex.printStackTrace();
		}
		return null;
	}

	@Override
	public Boleto select(Long key, String where) {
		return null;
	}

	@Override
	public void insert(SegmentoT segmento) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(SegmentoT segmento) {
		// TODO Auto-generated method stub
		
	}
}
