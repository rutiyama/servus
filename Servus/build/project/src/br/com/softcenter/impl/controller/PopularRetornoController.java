package br.com.softcenter.impl.controller;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.softcenter.impl.model.ETipoValidacao;
import br.com.softcenter.impl.model.IDAOContaMovimento;
import br.com.softcenter.impl.model.IDAOContaSaldoDia;
import br.com.softcenter.impl.model.IDAOModel;
import br.com.softcenter.impl.model.IDAOTituloBaixaImpl;
import br.com.softcenter.impl.model.IDAOTituloImpl;
import br.com.softcenter.impl.model.TipoSelecao;
import br.com.softcenter.impl.model.bean.ContaMovimento;
import br.com.softcenter.impl.model.bean.ContaSaldoDia;
import br.com.softcenter.impl.model.bean.Segmento;
import br.com.softcenter.impl.model.bean.SegmentoT;
import br.com.softcenter.impl.model.bean.Titulo;
import br.com.softcenter.impl.model.bean.TituloBaixa;
import br.com.softcenter.utils.database.DatabaseUtils;


/**
 * @author Rodrigo Junior Utiyama
 * @since  14-09-2015
 */

public class PopularRetornoController{
	private List<Segmento> segmentosImportados;
	private List<Segmento> segmentosNaoConformidade;
	
	private static final Long CON_ID = 18L;
	private int quantidadeDeRetornoInvalidoNaGravacao;
	private Long tit_id;
	
	public PopularRetornoController(List<Segmento> segmentos){
		this.segmentosImportados = segmentos;
	}
	
	/* Inicia a verificação dos títulos, para serem liquidados ou não: */
	private boolean validarCadaRetorno(SegmentoT segmentoT){
		boolean resultadoValidacao = false;
		
		if(segmentoT.getSegmentoU().getDataEfetivacaoCredito().before(DatabaseUtils.RetornarDataLimiteLancamentoFilial()) )
		{
			resultadoValidacao = true;
		}else
		if(segmentoT.getIdentificacaoNossoNumero() != null && !segmentoT.getIdentificacaoNossoNumero().equals(""))
		{
			String situacao = DatabaseUtils.RetornarSituacaoDoTitulo(Long.parseLong(segmentoT.getIdentificacaoNossoNumero()));
			if (situacao != null && situacao.trim().toLowerCase().equals("c"))
			{
				resultadoValidacao = true;
			}
		}
		
		return resultadoValidacao;
	}
	
	public void confirmarRetorno() throws Exception{
		
		List<String> duplicadosJaLiquidados = new ArrayList<String>();
		
		for(Segmento s : segmentosImportados){
			SegmentoT segmentoT = ((SegmentoT) s);
			boolean isNaoConformidade = false;
			boolean isDuplicado = false;
			
			/* Realiza as validações dos dados importados. Caso o resultado for verdadeiro,
			 * indica que o segmento atual não pôde ser importado. Portanto, pula o loop e
			 * não deixa realizar o insert do título no banco. (cancela o insert para este
			 * título):
			 */
			if(validarCadaRetorno(segmentoT)){
				++quantidadeDeRetornoInvalidoNaGravacao;
				continue;
			}
			
			/* Se o nosso número, do segmento, for vazio ou null, significa que
			 * o identificador é realizado pelo número da empresa, caso contrário,
			 * o identificador é realizado pelo nosso número (ou um ou outro):
			 */
//			try{
				
				for(Segmento stemp : segmentosNaoConformidade){
					if(((SegmentoT) stemp).getIdentificacaoNossoNumero().equals(segmentoT.getIdentificacaoNossoNumero())){
						isNaoConformidade = true;
						
						if(((SegmentoT) stemp).isDuplicado())
							isDuplicado = true;
						
						break;
					}
				}
				
				//Se está somente em não conformidade sem ser duplicado, pula a  integração
				if(isNaoConformidade && !isDuplicado)
					continue;
				else if(isNaoConformidade && isDuplicado){
					if(duplicadosJaLiquidados.indexOf(segmentoT.getIdentificacaoNossoNumero()) != -1){
						continue;
					}
				}
				
				if(segmentoT.getIdentificacaoNossoNumero() == null || segmentoT.getIdentificacaoNossoNumero().equals(""))
					validarRetornoNumeroEmpresa(segmentoT);
				else
					validarRetornoNossoNumero(segmentoT);
//			}catch(Exception ex){
//				ShowMessage.Exibir("Ocorreu um problema ao Inserir o título com o Nosso Número " + 
//							segmentoT.getIdentificacaoNossoNumero(), MensagemTipo.ERRO);
//				continue;
//			}
			
			if(isNaoConformidade && isDuplicado){
				duplicadosJaLiquidados.add(segmentoT.getIdentificacaoNossoNumero());
			}
		}
	}
	
	private void validarRetornoNumeroEmpresa(SegmentoT segmentoT) throws SQLException{
		System.out.println("Validando pelo Retorno do Número da Empresa.");
		iniciarValidacao(segmentoT, ETipoValidacao.ID_TITULO);
	}
	
	private void validarRetornoNossoNumero(SegmentoT segmentoT) throws SQLException{
		iniciarValidacao(segmentoT, ETipoValidacao.NOSSO_NUMERO);
	}
	
	private void iniciarValidacao(SegmentoT segmentoT, ETipoValidacao tipo) throws SQLException{
		IDAOModel<Titulo, Long> 		tituloDAO 		= new IDAOTituloImpl();
		IDAOModel<TituloBaixa, Integer> tituloBaixaDAO 	= new IDAOTituloBaixaImpl();
		Long l = DatabaseUtils.RetornarIdTituloPorNossoNumero(Long.parseLong(segmentoT.getIdentificacaoNossoNumero()));
		segmentoT.setTitID(l);
		
		//Primeiro, inserir os registros na tabela TituloBaixa:
		tituloBaixaDAO.insert(segmentoT);
		
		//Segundo, realizar o update na tabela, calculando o saldo:
		tituloDAO.update(segmentoT);
		
		//Terceiro, alterar a tabela TituloBaixa conforme o saldo calculado:
		tituloBaixaDAO.update(segmentoT);
		
		//Inserir na tabela de ContaMovimento:
		inserirContaMovimento(tituloDAO, segmentoT, tipo);
		
		//Atualizar Valores a Faturar:
		atualizarValoresAFaturar();
		
		if(segmentoT.getSegmentoU().getJurosMultasEncargos().doubleValue() > 0)
			inserirRegistroQuandoHaJuros(segmentoT, tipo);
	}
	
	public void inserirRegistroQuandoHaJuros(SegmentoT segmentoT, ETipoValidacao tipo) throws SQLException{
		
		IDAOModel<Titulo, Long> 		tituloDAO 		= new IDAOTituloImpl();
		IDAOModel<TituloBaixa, Integer> tituloBaixaDAO 	= new IDAOTituloBaixaImpl();
//		
//		//Primeiro, inserir os registros na tabela TituloBaixa:
		BigDecimal jurosTemp = segmentoT.getSegmentoU().getJurosMultasEncargos();
		BigDecimal valorTemp = segmentoT.getSegmentoU().getValorPagoSacado();
	
		segmentoT.getSegmentoU().setValorPagoSacado(jurosTemp);
		segmentoT.getSegmentoU().setJurosMultasEncargos(new BigDecimal(0));
		tituloBaixaDAO.insert(segmentoT);

		//Segundo, realizar o update na tabela, calculando o saldo:
		tituloDAO.update(segmentoT);
		
		//Terceiro, alterar a tabela TituloBaixa conforme o saldo calculado:
		tituloBaixaDAO.update(segmentoT);
		
		//Inserir na tabela de ContaMovimento:
		inserirContaMovimento(tituloDAO, segmentoT, tipo);
		
		//Atualizar Valores a Faturar:
		atualizarValoresAFaturar();
		
		segmentoT.getSegmentoU().setValorPagoSacado(valorTemp);
		segmentoT.getSegmentoU().setJurosMultasEncargos(jurosTemp);
	}
	
	private void inserirContaMovimento(IDAOModel<Titulo, Long> tituloDAO, SegmentoT segmentoT, ETipoValidacao tipo) throws SQLException{
		IDAOContaMovimento contaMovimentoDAO = new IDAOContaMovimento();
		
		switch(tipo){
			case NOSSO_NUMERO: tit_id = segmentoT.getTitID(); break; //DatabaseUtils.RetornarIdTituloPorNossoNumero(Long.parseLong(segmentoT.getIdentificacaoNossoNumero())); break;
			case ID_TITULO:	tit_id = DatabaseUtils.RetornarIdTituloPorIdentificaoBanco(segmentoT.getIdentificadorBanco());break;
		}
		
		if(tit_id == -1L)
			throw new SQLException();
		
		Titulo titulo = tituloDAO.select(tit_id);
		
		ContaMovimento contaMovimento = new ContaMovimento();
		contaMovimento.setDocumento(titulo.getNumeroTitulo());
		contaMovimento.setOrgID(titulo.getOrgID());
		contaMovimento.setTipo("C");
		contaMovimento.setTipoMovimento("A");
		contaMovimento.setValorSacado(segmentoT.getSegmentoU().getValorPagoSacado().subtract(segmentoT.getSegmentoU().getJurosMultasEncargos()));
		contaMovimento.setData(new Date());
		//contaMovimento.setSaldoDia(new BigDecimal()); verificar
		
		String tipoBaixa = "Parcial";
		if(segmentoT.getTipo().equals("Q"))
			tipoBaixa = "Total";
				
		String historico = "Movimento referente à baixa " + tipoBaixa + "  do Título à receber (" 
							+ titulo.getOrigem().getDescricao() + ") n° " + titulo.getNumeroTitulo() 
							+ ", parcela n° " + titulo.getParcela() + " e Cliente " + titulo.getCliente().getRazaoSocial();
		contaMovimento.setHistorico(historico);
		contaMovimentoDAO.insert(contaMovimento);
		
		//3°
		inserirContaSaldoDia(contaMovimento, segmentoT);
	}
	
	private void inserirContaSaldoDia(ContaMovimento contaMovimento,  SegmentoT segmentoT) throws SQLException{
		BigDecimal totalCredito = segmentoT.getSegmentoU().getValorPagoSacado();
		IDAOContaSaldoDia contaSDDAO = new IDAOContaSaldoDia();
		
		//Seleciona o registro que possui na mesma data e a mesma conta:
		ContaSaldoDia contaSaldoDia = contaSDDAO.select(contaMovimento.getData(), CON_ID);
		
		//Criar método para consultar o saldo acumulado antes da data específicada (data = contaMovimento.getData() id = CON_ID):
		BigDecimal saldoAcumuladoAtual = consultarSaldoAnteriorDataEspecificada(contaMovimento.getData(), contaSDDAO);
		
		//Se é nulo, significa que não há nenhum registro na tabela. Portanto, é necessário inserir:
		if (contaSaldoDia == null){
			contaSaldoDia = new ContaSaldoDia();
			contaSaldoDia.setContaID(CON_ID);
			contaSaldoDia.setData(contaMovimento.getData());
			contaSaldoDia.setTotalCredito(totalCredito);
			contaSaldoDia.setTotalDebito(new BigDecimal(0));
			contaSaldoDia.setSaldo(contaSaldoDia.getTotalCredito().subtract(contaSaldoDia.getTotalDebito()));
			contaSaldoDia.setSaldoAcumulado(contaSaldoDia.getSaldo().add(saldoAcumuladoAtual));
			saldoAcumuladoAtual = contaSaldoDia.getSaldo().add(saldoAcumuladoAtual);
			
			//Método inserir no banco...
			contaSDDAO.insert(contaSaldoDia);
		}else //Já existe registro na tabela
		{
			contaSaldoDia.setTotalCredito(contaSaldoDia.getTotalCredito().add(totalCredito));
			contaSaldoDia.setTotalDebito(contaSaldoDia.getTotalDebito().add(new BigDecimal(0)));
			contaSaldoDia.setSaldo(contaSaldoDia.getTotalCredito().subtract(contaSaldoDia.getTotalDebito()));
			contaSaldoDia.setSaldoAcumulado(contaSaldoDia.getSaldo().add(saldoAcumuladoAtual));
			saldoAcumuladoAtual = contaSaldoDia.getSaldo().add(saldoAcumuladoAtual);
			
			//Método atualizar no banco...
			contaSDDAO.update(contaSaldoDia);
		}
		
		//Loop para percorrer o saldo acumulado e atualizar os registros que possuem
		//a mesma conta e que sejam depois da data especificada:
		consultarSaldoPosteriorDataEspecificada(contaMovimento.getData(), contaSDDAO, saldoAcumuladoAtual);
	}
	
	/* Método responsável por calcular o saldo anterior até a data informada: */
	private BigDecimal consultarSaldoAnteriorDataEspecificada(Date data, IDAOContaSaldoDia contaSDDAO) throws SQLException{
		ContaSaldoDia contasSaldosDia = contaSDDAO.selectLimitOne(data, CON_ID, TipoSelecao.DATA_ANTERIOR);
		BigDecimal conta = new BigDecimal(0);
		
		if(contasSaldosDia != null)
			conta = contasSaldosDia.getSaldoAcumulado();	
			//conta = contasSaldosDia.get(contasSaldosDia.size()).getSaldoAcumulado();	
		
		return conta;
	}
	
	/* Método responsável por calcular o saldo anterior até a data informada: */
	private BigDecimal consultarSaldoPosteriorDataEspecificada(Date data, IDAOContaSaldoDia contaSDDAO, BigDecimal saldoAcumuladoAtual ) throws SQLException{
		List<ContaSaldoDia> contasSaldosDiaPosterior = contaSDDAO.selectAll(data, CON_ID, TipoSelecao.DATA_POSTERIOR);
		BigDecimal conta = new BigDecimal(0);
		
		if(contasSaldosDiaPosterior.size() > 0){
			for(ContaSaldoDia csd : contasSaldosDiaPosterior){
				csd.setSaldoAcumulado(csd.getSaldo().add(saldoAcumuladoAtual));
				saldoAcumuladoAtual = csd.getSaldo().add(saldoAcumuladoAtual);
				contaSDDAO.update(csd);
			}
		}
		return conta;
	}
	
	// Atualizar o campo VFT_SITUACAO para o valor "Q":
	private void atualizarValoresAFaturar() throws SQLException{
		DatabaseUtils.AtualizarTabelaValoresAFaturar(tit_id);
	}
	
	public int getQuantidadeRetornosInvalidosNaGravacao(){
		return quantidadeDeRetornoInvalidoNaGravacao;
	}

	public List<Segmento> getSegmentosNaoConformidade() {
		return segmentosNaoConformidade;
	}

	public void setSegmentosNaoConformidade(List<Segmento> segmentosNaoConformidade) {
		this.segmentosNaoConformidade = segmentosNaoConformidade;
	}
}
