package br.com.softcenter.impl.model.bean;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Rodrigo Junior Utiyama
 * @since  02-09-2015
 */


public class ContaBanco {
	private ObjectProperty<Conta> conta;
	private ObjectProperty<Banco> banco;
	private  SimpleStringProperty agencia;
	private  SimpleStringProperty contaNumero;
	
	public ContaBanco(Conta conta, Banco banco) {
	}
	public ContaBanco(){}
	
	public SimpleStringProperty agenciaProperty(){
		return agencia;
	}
	
	public SimpleStringProperty contaNumeroProperty(){
		return contaNumero;
	}
	
	public String getAgencia() {
		return agencia.get();
	}
	public String getContaNumero() {
		return contaNumero.get();
	}
	
	public void setAgencia(String agencia) {
		this.agencia = new SimpleStringProperty(agencia);
	}

	public void setContaNumero(String contaNumero) {
		this.contaNumero = new SimpleStringProperty(contaNumero);
	}

	public void setConta(Conta conta) {
		this.conta = new SimpleObjectProperty<Conta>(conta);
	}

	public void setBanco(Banco banco) {
		this.banco = new SimpleObjectProperty<Banco>(banco);
	}

	public ObjectProperty<Conta> getConta() {
		return conta;
	}

	public ObjectProperty<Banco> getBanco() {
		return banco;
	}
	@Override
	public String toString() {
		return "ContaBanco [conta=" + conta + ", banco=" + banco + ", agencia="
				+ agencia + ", contaNumero=" + contaNumero + "]";
	}
	
	
	
}
