package br.com.softcenter.impl.model.bean;

import java.math.BigDecimal;
import java.util.Date;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author Rodrigo Junior Utiyama
 * @since  09-09-2015
 */

public class SegmentoT extends Segmento{
	 //Nosso numero ou identificador de titulo da empresa
	private SimpleStringProperty identificacaoNossoNumero;
	private SimpleStringProperty identificadorBanco;
	private SimpleObjectProperty<SegmentoU> segmentoU;
	private BigDecimal saldo;
	private String tipo;
	//[Opcional] Utilizado quando um título em não conformidade for importado:
	private String observacao;
	private SimpleStringProperty nomeDoSacado;
	private String codigoMovimento;
	
	private SimpleStringProperty identificacaoSeuNumero;
	private SimpleObjectProperty<Date> dataVencimento;
	private SimpleObjectProperty<BigDecimal> valorDoTituloNominal;
	private Long titID;
	
	public SegmentoT(){
		identificacaoNossoNumero = new SimpleStringProperty();
		identificadorBanco 	     = new SimpleStringProperty();
		segmentoU 				 = new SimpleObjectProperty<SegmentoU>();
	}
	
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
     
	public SimpleStringProperty getIdentificadorBancoProperty(){
		return identificadorBanco;
	}

	public void setIdentificadorBanco(String identificadorBanco) {
		this.identificadorBanco = new SimpleStringProperty(identificadorBanco);
	}
	
	public void setIdentificadorBancoProperty(SimpleStringProperty identificadorBanco){
		this.identificadorBanco = identificadorBanco;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getIdentificadorBanco() {
		return identificadorBanco.get();
	}

	public void setIdentificacaoNossoNumero(
			SimpleStringProperty identificacaoNossoNumero) {
		this.identificacaoNossoNumero = identificacaoNossoNumero;
	}
	
	
	public String getNomeSacado(){
		return this.nomeDoSacado.get();
	}
	
	
	
	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public SimpleStringProperty getIdentificacaoNossoNumeroProperty(){
		return identificacaoNossoNumero;
	}
	
	public void setIdentificacaoNossoNumeroProperty(SimpleStringProperty identificacao){
		this.identificacaoNossoNumero = identificacao;
	}
	
	public String getIdentificacaoNossoNumero() {
		return identificacaoNossoNumero.get();
	}

	public void setIdentificacaoNossoNumero(String identificacao) {
		this.identificacaoNossoNumero = new SimpleStringProperty(identificacao);
	}
	
	public void setIdentificadorBanco(SimpleStringProperty identificadorBanco) {
		this.identificadorBanco = identificadorBanco;
	}
	
	public SegmentoU getSegmentoU() {
		return segmentoU.get();
	}

	public void setSegmentoU(SegmentoU segmentoU) {
		this.segmentoU = new SimpleObjectProperty<SegmentoU>(segmentoU);
	}

	
	public void setSegmentoUProperty(SimpleObjectProperty<SegmentoU> segmentoU){
		this.segmentoU = segmentoU;
	}
	
	public SimpleObjectProperty<SegmentoU> getSegmentoUProperty(){
		return this.segmentoU;
	}

	@Override
	public String toString() {
		return "SegmentoT [identificacaoNossoNumero="
				+ identificacaoNossoNumero + ", identificadorBanco="
				+ identificadorBanco + ", segmentoU=" + segmentoU + "]";
	}

	public SimpleStringProperty getNomeDoSacado() {
		return nomeDoSacado;
	}

	public void setNomeDoSacado(SimpleStringProperty nomeDoSacado) {
		this.nomeDoSacado = nomeDoSacado;
	}
	
	public void setNomeDoSacadoProperty(String nomeDoSacado) {
		this.nomeDoSacado = new SimpleStringProperty(nomeDoSacado);
	}

	public String getCodigoMovimento() {
		return codigoMovimento;
	}

	public void setCodigoMovimento(String codigoMovimento) {
		this.codigoMovimento = codigoMovimento;
	}

	public SimpleStringProperty getIdentificacaoSeuNumero() {
		return identificacaoSeuNumero;
	}

	public SimpleObjectProperty<Date> getDataVencimento() {
		return dataVencimento;
	}

	public SimpleObjectProperty<BigDecimal> getValorDoTituloNominal() {
		return valorDoTituloNominal;
	}

	public void setIdentificacaoSeuNumero(String identificacaoSeuNumero) {
		this.identificacaoSeuNumero = new SimpleStringProperty(identificacaoSeuNumero);
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = new SimpleObjectProperty<Date>(dataVencimento);
	}

	public void setValorDoTituloNominal(BigDecimal valorDoTituloNominal) {
		this.valorDoTituloNominal = new SimpleObjectProperty<BigDecimal>(valorDoTituloNominal);
	}

	public Long getTitID() {
		return titID;
	}

	public void setTitID(Long titID) {
		this.titID = titID;
	}
}
