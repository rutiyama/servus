package br.com.softcenter.impl.controller;

import java.util.List;

import br.com.softcenter.impl.model.bean.GridFaturasJavaFXBean;

/**
 * @author Rodrigo Junior Utiyama
 * @since  09-09-2015
 */

public class PopularGridArquivoRetornoFactory {
	public static void popularGrid(GridFaturasJavaFXBean gridFaturasBean, List<String>  duplicados){
			new PopularGridRetornoSegmentoT().popularGridRetornoJavaFx(gridFaturasBean, duplicados);
			new PopularGridRetornoSegmentoU().popularGridRetornoJavaFx(gridFaturasBean, duplicados);
	}
}
