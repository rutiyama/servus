package br.com.softcenter.impl.model;

public enum ETipoValidacao {
	NOSSO_NUMERO(1), 
	ID_TITULO(2);
	
	private int tipo;
	
	ETipoValidacao(int tipo){
		this.tipo = tipo;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
}
