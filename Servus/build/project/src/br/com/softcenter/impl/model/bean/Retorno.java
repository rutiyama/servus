package br.com.softcenter.impl.model.bean;

/**
 * @author Rodrigo Junior Utiyama
 * @since  10-09-2015
 */
public class Retorno {
	private Boleto boleto;
	private Titulo titulo;
	private TituloBaixa tituloBaixa;
	
	public Retorno(Boleto boleto, Titulo titulo, TituloBaixa tituloBaixa) {
		super();
		this.boleto = boleto;
		this.titulo = titulo;
		this.tituloBaixa = tituloBaixa;
	}
	
	public Boleto getBoleto() {
		return boleto;
	}
	public Titulo getTitulo() {
		return titulo;
	}
	public TituloBaixa getTituloBaixa() {
		return tituloBaixa;
	}
	public void setBoleto(Boleto boleto) {
		this.boleto = boleto;
	}
	public void setTitulo(Titulo titulo) {
		this.titulo = titulo;
	}
	public void setTituloBaixa(TituloBaixa tituloBaixa) {
		this.tituloBaixa = tituloBaixa;
	}

	@Override
	public String toString() {
		return "Retorno [boleto=" + boleto + ", titulo=" + titulo
				+ ", tituloBaixa=" + tituloBaixa + "]";
	}
}
