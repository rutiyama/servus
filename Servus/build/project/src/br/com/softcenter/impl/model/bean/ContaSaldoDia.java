package br.com.softcenter.impl.model.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since  15-09-2015
 */

public class ContaSaldoDia {
	private Long contaSDID;
	private Long contaID;
	private Date data;
	private BigDecimal totalDebito;
	private BigDecimal totalCredito;
	private BigDecimal saldo;
	private BigDecimal saldoAcumulado;
	
	public ContaSaldoDia(){}
	
	public Long getContaSDID() {
		return contaSDID;
	}
	public void setContaSDID(Long contaSDID) {
		this.contaSDID = contaSDID;
	}
	public Long getContaID() {
		return contaID;
	}
	public Date getData() {
		return data;
	}
	public BigDecimal getTotalDebito() {
		return totalDebito;
	}
	public BigDecimal getTotalCredito() {
		return totalCredito;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public BigDecimal getSaldoAcumulado() {
		return saldoAcumulado;
	}
	public void setContaID(Long contaID) {
		this.contaID = contaID;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setTotalDebito(BigDecimal totalDebito) {
		this.totalDebito = totalDebito;
	}
	public void setTotalCredito(BigDecimal totalCredito) {
		this.totalCredito = totalCredito;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	public void setSaldoAcumulado(BigDecimal saldoAcumulado) {
		this.saldoAcumulado = saldoAcumulado;
	}

	@Override
	public String toString() {
		return "ContaSaldoDia [contaID=" + contaID + ", data=" + data
				+ ", totalDebito=" + totalDebito + ", totalCredito="
				+ totalCredito + ", saldo=" + saldo + ", saldoAcumulado="
				+ saldoAcumulado + "]";
	}
	
	
}
